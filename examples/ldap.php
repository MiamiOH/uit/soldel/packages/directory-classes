<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/29/17
 * Time: 3:27 PM
 */

// The SDK class entries are autoloaded by composer
include_once __DIR__ . '/../vendor/autoload.php';

// The credentials and configuration can come from whatever source is
// appropriate for your deployment.
$config = parse_ini_file(__DIR__ . '/config.ini', true);

$ldap = \MiamiOH\Directory\MapperFactory::getLdapManager(new \MiamiOH\Directory\LdapConfiguration(
    $config['LDAP']['domain'],
    $config['LDAP']['url'],
    $config['LDAP']['dn'],
    $config['LDAP']['pw'],
    $config['LDAP']['basedn']
));

$ldapLoader = new \MiamiOH\Directory\EntryAttributesLoaderLdap($ldap);
$pNameLoader = new \MiamiOH\Directory\PreferredNameLoaderYaml(__DIR__ . '/names');
$mailboxLoader = new \MiamiOH\Directory\MailBoxLoaderRest();

$mapper = new \MiamiOH\Directory\EntryAttributesMapper($ldapLoader, $pNameLoader, $mailboxLoader);

$user = $mapper->getEntryByUniqueId('tepeds');

print "Got user entry for tepeds:\n";
print "First name: " . $user->getGivenName() . "\n";
print "Last name: " . $user->getFamilyName() . "\n";
print "Affiliations: " . print_r($user->getAffiliations(), true) . "\n";
print "Mail: " . $user->getMail() . "\n";
print "Mail aliases: " . print_r($user->getMailAliases(), true) . "\n";

$users = $mapper->getEntryListByUniqueIds(getUsers());

print "\nUsers\n";
foreach ($users as $user) {
    print $user->getGivenName() . ' ' . $user->getFamilyName() . ' (' . $user->getMail() . ")\n";
}

$user = $users->getByUniqueId('tepeds');
print "Found user " . $user->getGivenName() . "\n";

function getUsers() {
    return [
        'kodebopp',
        'vallamsd',
        'nanjals',
        'odonnepr',
        'huckemb',
        'ajitg',
        'zerini',
        'govekad',
        'bischoja',
        'zirklek',
        'kirklall',
        'trinhdh',
        'shettycm',
        'ojofa',
        'rajends',
        'ravendpp',
        'doqh',
        'achesocg',
        'tadh',
        'smitheg3',
        'garapaj',
        'patelah',
        'blackds2',
        'flenauj',
        'linebrc',
        'powersjp',
        'coliadg',
        'colenm',
        'travista',
        'maxweljd',
        'mundywp',
        'stilesws',
        'suchaneg',
        'hedrictj',
        'millse',
        'sennj',
        'natalejp',
        'liaom',
        'macklip2',
        'lackl',
        'kobyb',
        'deinr',
        'storerr',
        'schmidee',
        'olayasa',
        'kimbroe',
        'paguraa',
        'mille704',
        'kleinrf',
        'duhy',
        'brooksm8',
        'draked',
        'parrisna',
        'gengx',
        'robins64',
        'carterd2',
        'henebrbm',
        'raatzla',
        'xiaw',
        'stearnbk',
        'wallacdg',
        'hoovenej',
        'gollas',
        'kandasm',
        'amrheidk',
        'mileywg',
        'hurleycj',
        'roiwt',
        'luoy',
        'simmsbj',
        'supingmw',
        'mooresm',
        'emgeac',
        'wardtd',
        'kinnema',
        'rufft',
        'gruenhtt',
        'clarktw1',
        'harrisjd',
        'johns128',
        'schaefdm',
        'mikeserw',
        'bradlesw',
        'blackrw',
        'schroete',
        'schwindc',
        'hautauja',
        'tholesm',
        'hollowra',
        'dykescj',
        'ferrispc',
        'choutem',
        'madurorr',
        'bazeleje',
        'tuscanln',
        'wykoffpa',
        'mcdanic',
        'dillonm',
        'harrisde',
        'ferrenam',
        'cliftad',
        'beitzdk',
        'beckmasj',
        'lewisdw2',
        'beckmd',
        'gageha',
        'kidddw',
        'powellbc',
        'ritchem',
        'mcveyc',
        'farlerac',
        'davidgeb',
        'covertka',
        'tepeds',
        'bernarcj',
        'johnsoda',
        'henslel',
        'kellers3',
        'poleysa',
        'toaddyjm',
        'triplejl',
        'edestecd',
    ];
}