<?php
/**
 * Created by PhpStorm.
 * User: ravendpp
 * Date: 6/29/17
 * Time: 2:01 PM
 */

namespace MiamiOH\Directory;


interface MailBoxLoaderInterface
{
    public function getMailBoxInfoByUniqueId(string $uniqueId): MailBox;
    public function saveEntry(MailBox $entry): void;
}