<?php
/**
 * Created by PhpStorm.
 * User: shettycm
 * Date: 6/29/17
 * Time: 2:43 PM
 */

namespace MiamiOH\Directory;


use MiamiOH\Directory\Exception\EntryNotFoundException;


class AttributesCollection implements \Countable, \IteratorAggregate
{
    /**
     * @var AttributesInterface[]
     */
    private $attributes;

    /**
     * AttributesCollection constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes)
    {
        $this->ensure($attributes);
        $this->attributes = $attributes;
    }

    private function ensure(array $attributes): void
    {
        foreach ($attributes as $attribute) {
            if (!$attribute instanceof AttributesInterface) {
                throw new \InvalidArgumentException('AttributesLdap must be a decedent of ' . AttributesInterface::class);
            }
        }
    }

    public function getIterator(): AttributesCollectionIterator
    {
        return new AttributesCollectionIterator($this);
    }

    public function count(): int
    {
        return count($this->attributes);
    }

    public function toArray(): array
    {
        return $this->attributes;
    }
    
    public function addAttributes(AttributesLdap $attributes): void
    {
        $this->attributes[] = $attributes;
    }

    public function getByUniqueId(string $uniqueId): AttributesInterface
    {
        foreach ($this->attributes as $entry) {
            if ($entry->getUniqueId() === $uniqueId) {
                return $entry;
            }
        }

        throw new EntryNotFoundException();
    }
}