<?php

namespace MiamiOH\Directory\Tests;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use MiamiOH\Directory\MailBox;
use MiamiOH\Directory\MailBoxLoaderRestng;
use MiamiOH\Directory\RestConfiguration;
use PHPUnit\Framework\TestCase;

class MailBoxLoaderRestngTest extends TestCase
{
    private $container = [];
    /**
     * @var MailBoxLoaderRestng
     */
    private $mailBoxLoader;


    /**
     * @var RestConfiguration
     */
    private $configuration;

    protected function setUp(): void
    {
        $this->configuration = new RestConfiguration('https://example.com',
            'testUser', 'testPassword');
    }
    public function testReturnMailBoxForGivenUniqueId()
    {
        $this->mailBoxLoader = new MailBoxLoaderRestng(new Client, $this->configuration);
        $this->assertInstanceOf(MailBox::class,
            $this->mailBoxLoader->getMailBoxInfoByUniqueId('bob'));
    }

    public function testReturnCorrectUniqueId()
    {
        $this->mailBoxLoader = new MailBoxLoaderRestng(new Client, $this->configuration);
        $mailBox = $this->mailBoxLoader->getMailBoxInfoByUniqueId('bob');
        $this->assertEquals('bob', $mailBox->getUniqueId());
    }

    //test nothing happens when a clean mailbox received
    public function testNoActionIsTakenForCleanMailBox()
    {
        $mailbox = new MailBox('bob',[]);
        $this->mailBoxLoader = new MailBoxLoaderRestng($this->newHttpClientWithResponses([]), $this->configuration);
        $this->mailBoxLoader->saveEntry($mailbox);
        $this->assertEmpty($this->container);
    }

    public function testAliasesAreSaved()
    {
        $expectedToken = 'efg456';
        $expectedUniqueID = 'bob';
        $mailbox = new MailBox($expectedUniqueID,[]);
        $mailbox->setMailAliases(['bobNew@miamioh.edu']);
        $mailbox->setPreferredName('bobtest');
        $mailbox->setFamilyName('bobFamily');
        $responses = [
            $this->newTokenResponse($expectedToken),
            $this->newJsonResponse($this->makeRestngDataResponse([
                'owner'=> $expectedUniqueID,
                'nickname'=> ['testNickname']
            ])
            ),
            $this->newJsonResponse($this->makeRestngDataResponse([
                'sendas_addresses'=> [
                    [
                        'address'=> "testUser@gdev.miamioh.edu"
                    ],
                    [
                        'address' => 'testUser2@gdev.miamioh.edu'
                    ]
                ]
            ])),
            $this->newJsonResponse($this->makeRestngDataResponse([
                [
                    'owner'=> $expectedUniqueID,
                    'nickname'=> 'testNickname'
                ]
            ])),
            $this->newJsonResponse($this->makeRestngDataResponse([
                [
                    'name' => 'bob',
                    'address' => 'bobNew@miamioh.edu',
                    'replyTo' => 'bob2@miamioh.edu',
                    'makeDefault'=> true
                ]
            ])),
            $this->newJsonResponse($this->makeRestngDataResponse([]))
        ];
        $client = $this->newHttpClientWithResponses($responses);
        $this->mailBoxLoader = new MailBoxLoaderRestng($client, $this->configuration, ['miamioh.edu']);

        $this->mailBoxLoader->saveEntry($mailbox);

        $this->assertCount(count($responses), $this->container);

        /** @var Request $resourceRequest */
        $resourceRequest = $this->container[0]['request'];

        $this->assertEquals('/api/authentication/v1', (string)$resourceRequest->getUri());
        $this->assertContains('application/json', $resourceRequest->getHeader('content-type'));

        $body = json_decode($resourceRequest->getBody()->getContents(), true);
        $this->assertArrayHasKey('username', $body);
        $this->assertArrayHasKey('password', $body);
        $this->assertArrayHasKey('type', $body);

        //GET request for nickname
        $resourceRequest = $this->container[1]['request'];

        $this->assertEquals(
            '/api/googleApps/user/nicknames/v3/' .$expectedUniqueID.'?token=' .$expectedToken,
            (string)$resourceRequest->getUri()
        );
        $this->assertEquals('GET',$resourceRequest->getMethod());

        //GET request for emailsettings
        $resourceRequest = $this->container[2]['request'];

        $this->assertEquals(
            '/api/googleApps/emailSettings/v3/' .$expectedUniqueID.'/sendas?token=' .$expectedToken,
            (string)$resourceRequest->getUri()
        );
        $this->assertEquals('GET',$resourceRequest->getMethod());

        //POST request for nickname
        $resourceRequest = $this->container[3]['request'];

        $this->assertEquals(
            '/api/googleApps/nickname/v3?token=' .$expectedToken,
            (string)$resourceRequest->getUri()
        );
        $this->assertEquals('POST',$resourceRequest->getMethod());

        //POST request for emailsettings
        $resourceRequest = $this->container[4]['request'];

        $this->assertEquals(
            '/api/googleApps/emailSettings/v3/'. $expectedUniqueID .'/sendas?token=' .$expectedToken,
            (string)$resourceRequest->getUri()
        );
        $this->assertEquals('POST',$resourceRequest->getMethod());

        //Delete request for nickname
        $resourceRequest = $this->container[5]['request'];

        $this->assertEquals(
            '/api/googleApps/nickname/v3/testNickname?token=' .$expectedToken,
            (string)$resourceRequest->getUri()
        );
        $this->assertEquals('DELETE',$resourceRequest->getMethod());
    }


    private function newHttpClientWithResponses(array $responses): Client
    {
        $mock = new MockHandler($responses);

        $this->container = [];
        $history = Middleware::history($this->container);

        $handler = HandlerStack::create($mock);
        $handler->push($history);

        return new Client(['handler' => $handler]);
    }

    private function newTokenResponse(string $token = 'abc123', string $username = 'bubba', Carbon $expires = null): Response
    {
        if (null === $expires) {
            $expires = Carbon::now()->addMinutes(60);
        }

        return new Response(
            200,
            [
                'content-type' => 'application/json',
                'content-length' => '10'
            ],
            json_encode([
                'data' => [
                    'token' => $token,
                    'username' => $username,
                    'tokenLifetime' => $expires->toW3cString(),
                ],
                'status' => 200,
                'error' => false,
            ])
        );
    }

    private function newJsonResponse(array $content, int $status = 200, array $headers = []): Response
    {
        $body = json_encode($content);

        if (empty($headers)) {
            $headers['content-type'] = 'application/json';
            $headers['content-length'] = strlen($body);
        }

        return new Response(
            $status,
            $headers,
            $body
        );
    }

    private function makeRestngDataResponse(array $data, int $status=200, string $error=''): array
    {
        return [
            'data' => $data,
            'status' => $status,
            'error' => $error
        ];
    }

}
