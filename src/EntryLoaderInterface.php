<?php


interface EntryLoaderInterface
{
    public function getDirectoryEntry();
}