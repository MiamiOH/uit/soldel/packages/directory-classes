<?php
/**
 * Created by PhpStorm.
 * User: shettycm
 * Date: 6/30/17
 * Time: 1:09 PM
 */

namespace MiamiOH\Directory\Tests;


use MiamiOH\Directory\RestConfiguration;
use PHPUnit\Framework\TestCase;


class RestConfigurationTest extends TestCase
{
    /**
     * @var RestConfiguration
     */
    private $configuration;

    private $baseUrl = '';
    private $username = '';
    private $password = '';

    public function setUp(): void
    {
        $this->baseUrl = 'http://wsdev.miamioh.edu';
        $this->username = 'username';
        $this->password = 'password';

        $this->configuration = new RestConfiguration($this->baseUrl, $this->username, $this->password);
    }

    public function testCanBeCreatedWithBaseUrl(): void
    {
        $this->assertInstanceOf(RestConfiguration::class, new RestConfiguration($this->baseUrl));
    }

    public function testCanGetBaseUrl(): void
    {
        $this->assertEquals($this->baseUrl, $this->configuration->getBaseUrl());
    }

    public function testCanGetUsername(): void
    {
        $this->assertEquals($this->username, $this->configuration->getUsername());
    }

    public function testCanGetPassword(): void
    {
        $this->assertEquals($this->password, $this->configuration->getPassword());
    }
}
