<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/29/17
 * Time: 10:23 AM
 */

namespace MiamiOH\Directory\Tests;

use LdapTools\LdapManager;
use LdapTools\Object\LdapObject;
use LdapTools\Object\LdapObjectCollection;
use LdapTools\Query\LdapQuery;
use LdapTools\Query\LdapQueryBuilder;
use MiamiOH\Directory\AttributesLdap;
use MiamiOH\Directory\AttributesCollection;
use MiamiOH\Directory\EntryAttributesLoaderLdap;
use PHPUnit\Framework\TestCase;

class EntryAttributesLoaderLdapTest extends TestCase
{
    /**
     * @var EntryAttributesLoaderLdap
     */
    private $loader;
    /**
     * @var LdapManager
     */
    private $ldapManager;
    /**
     * @var LdapQueryBuilder
     */
    private $ldapQueryBuilder;
    /**
     * @var LdapQuery
     */
    private $ldapQuery;
    
    protected function setUp(): void
    {
        $this->ldapManager = $this->getMockBuilder(LdapManager::class)
            ->disableOriginalConstructor()
            ->setMethods(['buildLdapQuery'])
            ->getMock();

        $this->ldapQueryBuilder = $this->getMockBuilder(LdapQueryBuilder::class)
            ->disableOriginalConstructor()
            ->setMethods(['select', 'where', 'orWhere', 'getLdapQuery'])
            ->getMock();

        $this->ldapQuery = $this->getMockBuilder(LdapQuery::class)
            ->disableOriginalConstructor()
            ->setMethods(['getSingleResult', 'getResult'])
            ->getMock();

        $this->ldapManager->method('buildLdapQuery')->willReturn($this->ldapQueryBuilder);
        $this->ldapQueryBuilder->method('select')->will($this->returnSelf());
        $this->ldapQueryBuilder->method('where')->will($this->returnSelf());
        $this->ldapQueryBuilder->method('orWhere')->will($this->returnSelf());
        $this->ldapQueryBuilder->method('getLdapQuery')->willReturn($this->ldapQuery);

        $this->loader = new EntryAttributesLoaderLdap($this->ldapManager);
    }

    public function testCanBeCreated(): void
    {
        $this->assertInstanceOf(EntryAttributesLoaderLdap::class, $this->loader);
    }

    public function testCanFindEntryByUniqueId(): void
    {
        $ldapEntry = new LdapObject();
        $ldapEntry->set('uid', 'bob');
        $ldapEntry->set('givenName', 'Bob');
        $ldapEntry->set('sn', 'Smith');
        $ldapEntry->set('muohioeduPrimaryAffiliationCode', 'sta');
        $ldapEntry->set('muohioeduAffiliationCode', ['sta', 'ins']);

        $this->ldapQuery->method('getSingleResult')->willReturn($ldapEntry);

        $user = $this->loader->findEntryByUniqueId('bob');
        $this->assertInstanceOf(AttributesLdap::class, $user);
        $this->assertEquals('Bob', $user->getGivenName());
        $this->assertEquals('Smith', $user->getFamilyName());
    }

    public function testCanFindEntryByUniqueIdCollection(): void
    {
        $ldapEntry = new LdapObject();
        $ldapEntry->set('uid', 'bob');
        $ldapEntry->set('givenName', 'Bob');
        $ldapEntry->set('sn', 'Smith');
        $ldapEntry->set('muohioeduPrimaryAffiliationCode', 'sta');
        $ldapEntry->set('muohioeduAffiliationCode', ['sta', 'ins']);

        $this->ldapQuery->method('getResult')->willReturn(new LdapObjectCollection($ldapEntry));

        $this->assertInstanceOf(AttributesCollection::class, $this->loader->findEntriesByUniqueIdArray(['bob']));
    }

}
