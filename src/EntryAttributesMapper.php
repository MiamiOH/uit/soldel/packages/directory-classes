<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/29/17
 * Time: 10:28 AM
 */

namespace MiamiOH\Directory;


use MiamiOH\Directory\Exception\EntryNotFoundException;

class EntryAttributesMapper
{

    /**
     * @var EntryAttributesLoaderInterface
     */
    private $attributeLoader;
    /**
     * @var PersonDirectoryPreferencesLoaderInterface
     */
    private $personLoader;

    public function __construct(
        EntryAttributesLoaderInterface $attributeLoader,
        PersonDirectoryPreferencesLoaderInterface $personLoader,
        MailBoxLoaderInterface $mailBoxLoader
    )
    {
        $this->attributeLoader = $attributeLoader;
        $this->personLoader = $personLoader;
        $this->mailBoxLoader = $mailBoxLoader;
    }

    public function getEntryByUniqueId(string $uniqueId): EntryAttributes
    {
        $attributes = $this->attributeLoader->findEntryByUniqueId($uniqueId);
        $personDirectoryPreferences = $this->personLoader->getPersonDirectoryPreferencesByUniqueId($uniqueId);
        $mailBox = $this->mailBoxLoader->getMailBoxInfoByUniqueId($uniqueId);
        

        // Construct entry from data
        return new EntryAttributes($attributes, $personDirectoryPreferences, $mailBox);
    }

    public function getEntryListByUniqueIds(array $idList): EntryAttributesCollection
    {
        $entriesAttributes = $this->attributeLoader->findEntriesByUniqueIdArray($idList);

        $entries = [];
        foreach ($idList as $id) {
            try {
                $attributes = $entriesAttributes->getByUniqueId($id);
            } catch (EntryNotFoundException $e) {
                $attributes = new AttributesArray(['uid' => $id]);
            }

            $entries[] = new EntryAttributes($attributes);
        }

        return new EntryAttributesCollection($entries);
    }

    public function saveEntry(EntryAttributes $entry): void
    {
        $this->attributeLoader->saveEntry($entry->getAttributesContainer());
        $this->personLoader->savePersonDirectoryPreferencesEntry(
            $entry->getPersonDirectoryPreferencesContainer(),
            $entry->getAttributesContainer()
        );
        $this->mailBoxLoader->saveEntry($entry->getMailBoxContainer());
    }

    public function checkEmailAliases(array $aliases): array
    {
        return $this->personLoader->checkEmailAliases($aliases);
    }

    public function recordEmailAliases(string $uniqueId, array $data): void
    {
        $this->personLoader->recordEmailAliases($uniqueId, $data);
    }

}
