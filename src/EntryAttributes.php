<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/29/17
 * Time: 10:30 AM
 */

namespace MiamiOH\Directory;


class EntryAttributes
{
    /**
     * @var PersonDirectoryPreferences
     */
    private $personDirectoryPreferences;

    /**
     * @var AttributesInterface
     */
    private $attributes;

    /**
     * @var MailBox
     */
    private $mailBoxInfo;
    

    public function __construct(AttributesInterface $attributes, PersonDirectoryPreferences $personDirectoryPreferences = null, MailBox $mailBoxInfo = null)
    {
        $this->personDirectoryPreferences = $personDirectoryPreferences;
        $this->attributes = $attributes;
        $this->mailBoxInfo = $mailBoxInfo;
    }

    public function getAttributesContainer(): AttributesInterface
    {
        return $this->attributes;
    }

    public function getPersonDirectoryPreferencesContainer(): PersonDirectoryPreferences
    {
        return $this->personDirectoryPreferences;
    }
    
    public function getPreferredName(): string
    {
        return $this->personDirectoryPreferences->getPreferredName();

    }

    public function setPreferredName(string $data): void
    {
        $this->personDirectoryPreferences->setPreferredName($data);
    }

    public function getHideMiddleName(): string
    {
        return $this->personDirectoryPreferences->getHideMiddleName();
    }

    public function setHideMiddleName(string $data): void
    {
        $this->personDirectoryPreferences->setHideMiddleName($data);
    }

    public function getDirectoryListing(): string
    {
        return $this->personDirectoryPreferences->getDirectoryListing();
    }

    public function setDirectoryListing(string $data): void
    {
        $this->personDirectoryPreferences->setDirectoryListing($data);
    }
    
    public function getMailBoxContainer(): MailBox
    {
        return $this->mailBoxInfo;
    }

    public function getBuildingNames(): array
    {
        return $this->attributes->getBuildingNames();
    }

    public function setBuildingNames(array $data): void
    {
        $this->attributes->setBuildingNames($data);
    }

    public function getFaxNumber(): string
    {
        return $this->attributes->getFaxNumber();
    }

    public function setFaxNumber(string $data): void
    {
        $this->attributes->setFaxNumber($data);
    }

    public function getNameSuffix(): string
    {
        return $this->attributes->getNameSuffix();
    }

    public function setNameSuffix(string $data): void
    {
        $this->attributes->setNameSuffix($data);
    }

    public function getGivenName(): string
    {
        return $this->attributes->getGivenName();
    }

    public function setGivenName(string $data): void
    {
        $this->attributes->setGivenName($data);
    }

    public function getHomePhone(): array
    {
        return $this->attributes->getHomePhone();
    }

    public function setHomePhone(array $data): void
    {
        $this->attributes->setHomePhone($data);
    }

    public function getHomePostalAddress(): PostalAddress
    {
        return $this->attributes->getHomePostalAddress();
    }

    public function setHomePostalAddress(PostalAddress $data): void
    {
        $this->attributes->setHomePostalAddress($data);
    }

    public function getLocations(): array
    {
        return $this->attributes->getLocations();
    }

    public function setLocations(array $data): void
    {
        $this->attributes->setLocations($data);
    }

    public function getLabeledUri(): string
    {
        return $this->attributes->getLabeledUri();
    }

    public function setLabeledUri(string $data): void
    {
        $this->attributes->setLabeledUri($data);
    }

    public function getMail(): string
    {
        return $this->attributes->getMail();
    }

    public function setMail(string $data): void
    {
        $this->attributes->setMail($data);
    }

    public function getMailAliases(): array
    {
        return $this->attributes->getMailAliases();
    }

    public function setMailAliases(array $data): void
    {
        $this->attributes->setMailAliases($data);
    }
    /**
     *  For Google Mailbox and nicknames
     */
    public function setMailBoxInfo(array $data): void
    {
        $this->mailBoxInfo->setPreferredName($data['preferredName']);
        $this->mailBoxInfo->setFamilyName($data['familyName']);
        $this->mailBoxInfo->setMailAliases($data);
    }

    public function getMailRoutingAddress(): string
    {
        return $this->attributes->getMailRoutingAddress();
    }

    public function setMailRoutingAddress(string $data): void
    {
        $this->attributes->setMailRoutingAddress($data);
    }

    public function getMobileNumber(): string
    {
        return $this->attributes->getMobileNumber();
    }

    public function setMobileNumber(string $data): void
    {
        $this->attributes->setMobileNumber($data);
    }

    public function getAdditionalAffiliations(): array
    {
        return $this->attributes->getAdditionalAffiliations();
    }

    public function setAdditionalAffiliations(array $data): void
    {
        $this->attributes->setAdditionalAffiliations($data);
    }

    public function getAffiliations(): array
    {
        return $this->attributes->getAffiliations();
    }

    public function setAffiliations(array $data): void
    {
        $this->attributes->setAffiliations($data);
    }

    public function getAffiliationCodes(): array
    {
        return $this->attributes->getAffiliationCodes();
    }

    public function setAffiliationCodes(array $data): void
    {
        $this->attributes->setAffiliationCodes($data);
    }

    public function getBuildingCodes(): array
    {
        return $this->attributes->getBuildingCodes();
    }

    public function setBuildingCodes(array $data): void
    {
        $this->attributes->setBuildingCodes($data);
    }

    public function getCampusTelephoneNumber(): string
    {
        return $this->attributes->getCampusTelephoneNumber();
    }

    public function setCampusTelephoneNumber(string $data): void
    {
        $this->attributes->setCampusTelephoneNumber($data);
    }

    public function getClasses(): array
    {
        return $this->attributes->getClasses();
    }

    public function setClasses(array $data): void
    {
        $this->attributes->setClasses($data);
    }

    public function getClassCodes(): array
    {
        return $this->attributes->getClassCodes();
    }

    public function setClassCodes(array $data): void
    {
        $this->attributes->setClassCodes($data);
    }

    public function getColleges(): array
    {
        return $this->attributes->getColleges();
    }

    public function setColleges(array $data): void
    {
        $this->attributes->setColleges($data);
    }

    public function getCollegeCodes(): array
    {
        return $this->attributes->getCollegeCodes();
    }

    public function setCollegeCodes(array $data): void
    {
        $this->attributes->setCollegeCodes($data);
    }

    public function getDepartments(): array
    {
        return $this->attributes->getDepartments();
    }

    public function setDepartments(array $data): void
    {
        $this->attributes->setDepartments($data);
    }

    public function getDepartmentCodes(): array
    {
        return $this->attributes->getDepartmentCodes();
    }

    public function setDepartmentCodes(array $data): void
    {
        $this->attributes->setDepartmentCodes($data);
    }

    public function getDirectoryProxies(): array
    {
        return $this->attributes->getDirectoryProxies();
    }

    public function setDirectoryProxies(array $data): void
    {
        $this->attributes->setDirectoryProxies($data);
    }

    public function getDivisions(): array
    {
        return $this->attributes->getDivisions();
    }

    public function setDivisions(array $data): void
    {
        $this->attributes->setDivisions($data);
    }

    public function getDivisionCodes(): array
    {
        return $this->attributes->getDivisionCodes();
    }

    public function setDivisionCodes(array $data): void
    {
        $this->attributes->setDivisionCodes($data);
    }

    public function getEmployeeSchools(): array
    {
        return $this->attributes->getEmployeeSchools();
    }

    public function setEmployeeSchools(array $data): void
    {
        $this->attributes->setEmployeeSchools($data);
    }

    public function getEmployeeSchoolCodes(): array
    {
        return $this->attributes->getEmployeeSchoolCodes();
    }

    public function setEmployeeSchoolCodes(array $data): void
    {
        $this->attributes->setEmployeeSchoolCodes($data);
    }

    public function getEmployeeType(): string
    {
        return $this->attributes->getEmployeeType();
    }

    public function setEmployeeType(string $data): void
    {
        $this->attributes->setEmployeeType($data);
    }

    public function getEmployeeTypeCode(): string
    {
        return $this->attributes->getEmployeeTypeCode();
    }

    public function setEmployeeTypeCode(string $data): void
    {
        $this->attributes->setEmployeeTypeCode($data);
    }

    public function getGreekOrg(): string
    {
        return $this->attributes->getGreekOrg();
    }

    public function setGreekOrg(string $data): void
    {
        $this->attributes->setGreekOrg($data);
    }

    public function getHidden(): string
    {
        return $this->attributes->getHidden();
    }

    public function setHidden(string $data): void
    {
        $this->attributes->setHidden($data);
    }

    public function getHigherEducation(): string
    {
        return $this->attributes->getHigherEducation();
    }

    public function setHigherEducation(string $data): void
    {
        $this->attributes->setHigherEducation($data);
    }

    public function getHighschoolAttended(): string
    {
        return $this->attributes->getHighschoolAttended();
    }

    public function setHighschoolAttended(string $data): void
    {
        $this->attributes->setHighschoolAttended($data);
    }

    public function getHours(): string
    {
        return $this->attributes->getHours();
    }

    public function setHours(string $data): void
    {
        $this->attributes->setHours($data);
    }

    public function getLabeledPhotoURL(): string
    {
        return $this->attributes->getLabeledPhotoURL();
    }

    public function setLabeledPhotoURL(string $data): void
    {
        $this->attributes->setLabeledPhotoURL($data);
    }

    public function getLocationCodes(): array
    {
        return $this->attributes->getLocationCodes();
    }

    public function setLocationCodes(array $data): void
    {
        $this->attributes->setLocationCodes($data);
    }

    public function getMailStopCodes(): array
    {
        return $this->attributes->getMailStopCodes();
    }

    public function setMailStopCodes(array $data): void
    {
        $this->attributes->setMailStopCodes($data);
    }

    public function getMajors(): array
    {
        return $this->attributes->getMajors();
    }

    public function setMajors(array $data): void
    {
        $this->attributes->setMajors($data);
    }

    public function getMajorCodes(): array
    {
        return $this->attributes->getMajorCodes();
    }

    public function setMajorCodes(array $data): void
    {
        $this->attributes->setMajorCodes($data);
    }

    public function getMesgAddr(): string
    {
        return $this->attributes->getMesgAddr();
    }

    public function setMesgAddr(string $data): void
    {
        $this->attributes->setMesgAddr($data);
    }

    public function getMiddleInitial(): string
    {
        return $this->attributes->getMiddleInitial();
    }

    public function setMiddleInitial(string $data): void
    {
        $this->attributes->setMiddleInitial($data);
    }

    public function getMiddleName(): string
    {
        return $this->attributes->getMiddleName();
    }

    public function setMiddleName(string $data): void
    {
        $this->attributes->setMiddleName($data);
    }

    public function getMinors(): array
    {
        return $this->attributes->getMinors();
    }

    public function setMinors(array $data): void
    {
        $this->attributes->setMinors($data);
    }

    public function getMinorCodes(): array
    {
        return $this->attributes->getMinorCodes();
    }

    public function setMinorCodes(array $data): void
    {
        $this->attributes->setMinorCodes($data);
    }

    public function getPagerEmail(): string
    {
        return $this->attributes->getPagerEmail();
    }

    public function setPagerEmail(string $data): void
    {
        $this->attributes->setPagerEmail($data);
    }

    public function getPhotoURL(): string
    {
        return $this->attributes->getPhotoURL();
    }

    public function setPhotoURL(string $data): void
    {
        $this->attributes->setPhotoURL($data);
    }

    public function getPrimaryAffiliation(): string
    {
        return $this->attributes->getPrimaryAffiliation();
    }

    public function setPrimaryAffiliation(string $data): void
    {
        $this->attributes->setPrimaryAffiliation($data);
    }

    public function getPrimaryAffiliationCode(): string
    {
        return $this->attributes->getPrimaryAffiliationCode();
    }

    public function setPrimaryAffiliationCode(string $data): void
    {
        $this->attributes->setPrimaryAffiliationCode($data);
    }

    public function getPrimaryLocation(): string
    {
        return $this->attributes->getPrimaryLocation();
    }

    public function setPrimaryLocation(string $data): void
    {
        $this->attributes->setPrimaryLocation($data);
    }

    public function getPrimaryLocationCode(): string
    {
        return $this->attributes->getPrimaryLocationCode();
    }

    public function setPrimaryLocationCode(string $data): void
    {
        $this->attributes->setPrimaryLocationCode($data);
    }

    public function getProjects(): string
    {
        return $this->attributes->getProjects();
    }

    public function setProjects(string $data): void
    {
        $this->attributes->setProjects($data);
    }

    public function getRecordInactive(): string
    {
        return $this->attributes->getRecordInactive();
    }

    public function setRecordInactive(string $data): void
    {
        $this->attributes->setRecordInactive($data);
    }

    public function getVPNGroup(): string
    {
        return $this->attributes->getVPNGroup();
    }

    public function setVPNGroup(string $data): void
    {
        $this->attributes->setVPNGroup($data);
    }

    public function getPagerNumber(): string
    {
        return $this->attributes->getPagerNumber();
    }

    public function setPagerNumber(string $data): void
    {
        $this->attributes->setPagerNumber($data);
    }

    public function getNamePrefix(): string
    {
        return $this->attributes->getNamePrefix();
    }

    public function setNamePrefix(string $data): void
    {
        $this->attributes->setNamePrefix($data);
    }

    public function getPostalAddress(): PostalAddress
    {
        return $this->attributes->getPostalAddress();
    }

    public function setPostalAddress(PostalAddress $data): void
    {
        $this->attributes->setPostalAddress($data);
    }

    public function getFamilyName(): string
    {
        return $this->attributes->getFamilyName();
    }

    public function setFamilyName(string $data): void
    {
        $this->attributes->setFamilyName($data);
    }

    public function getTelephoneNumber(): array
    {
        return $this->attributes->getTelephoneNumber();
    }

    public function setTelephoneNumber(array $data): void
    {
        $this->attributes->setTelephoneNumber($data);
    }

    public function getTitles(): array
    {
        return $this->attributes->getTitles();
    }

    public function setTitles(array $data): void
    {
        $this->attributes->setTitles($data);
    }

    public function getUniqueId(): string
    {
        return $this->attributes->getUniqueId();
    }

    public function setUniqueId(string $data): void
    {
        $this->attributes->setUniqueId($data);
    }

    public function getDisplayName(): string
    {
        return $this->attributes->getDisplayName();
    }

    public function setDisplayName(string $data): void
    {
        $this->attributes->setDisplayName($data);
    }

    public function getEntitlements(): array
    {
        return $this->attributes->getEntitlements();
    }

    public function setEntitlements(array $data): void
    {
        $this->attributes->setEntitlements($data);
    }

}
