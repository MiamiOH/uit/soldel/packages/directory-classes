<?php

namespace MiamiOH\Directory\App\Util;

use MiamiOH\Directory\App\Util\Log;

/**
 * Class NullLog
 * @package MiamiOH\Directory\App
 *
 * @codeCoverageIgnore
 */
class NullLog implements Log
{
    private static $quiet = true;

    /**
     * @inheritDoc
     */
    public function emergency($message, array $context = [])
    {
        if (self::$quiet) {
            return;
        }
    }

    /**
     * @inheritDoc
     */
    public function alert($message, array $context = [])
    {
        if (self::$quiet) {
            return;
        }
    }

    /**
     * @inheritDoc
     */
    public function critical($message, array $context = [])
    {
        if (self::$quiet) {
            return;
        }
    }

    /**
     * @inheritDoc
     */
    public function error($message, array $context = [])
    {
        if (self::$quiet) {
            return;
        }
    }

    /**
     * @inheritDoc
     */
    public function warning($message, array $context = [])
    {
        if (self::$quiet) {
            return;
        }
    }

    /**
     * @inheritDoc
     */
    public function notice($message, array $context = [])
    {
        if (self::$quiet) {
            return;
        }
    }

    /**
     * @inheritDoc
     */
    public function info($message, array $context = [])
    {
        if (self::$quiet) {
            return;
        }
    }

    /**
     * @inheritDoc
     */
    public function debug($message, array $context = [])
    {
        if (self::$quiet) {
            return;
        }
    }

    /**
     * @inheritDoc
     */
    public function log($level, $message, array $context = [])
    {
        if (self::$quiet) {
            return;
        }
    }
}
