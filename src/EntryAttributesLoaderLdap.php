<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/29/17
 * Time: 10:22 AM
 */

namespace MiamiOH\Directory;


use LdapTools\LdapManager;
use LdapTools\Object\LdapObject;
use MiamiOH\Directory\Exception\LdapWriteNotConfiguredException;

class EntryAttributesLoaderLdap implements EntryAttributesLoaderInterface
{
    
    /**
     * @var LdapManager
     */
    private $ldapManagerRead;
    /**
     * @var LdapManager
     */
    private $ldapManagerWrite;

    private static $shortAttributeList = [
        'givenName',
        'mail',
        'muohioeduDepartment',
        'muohioeduPrimaryAffiliation',
        'sn',
        'telephoneNumber',
        'title',
        'uid',
    ];

    private static $fullAttributeList = [
        'buildingName',
        'cn',
        'facsimileTelephoneNumber',
        'generationQualifier',
        'givenName',
        'l',
        'labeledUri',
        'mail',
        'mailAlternateAddress',
        'mailRoutingAddress',
        'mobile',
        'muohioeduCampusTelephoneNumber',
        'muohioeduClass',
        'muohioeduCollege',
        'muohioeduDepartment',
        'muohioeduDirectoryProxy',
        'muohioeduDivision',
        'muohioeduEmployeeSchool',
        'muohioeduEmployeeType',
        'muohioeduGreekOrg',
        'muohioeduHidden',
        'muohioeduHigherEducation',
        'muohioeduHighschoolAttended',
        'muohioeduHours',
        'muohioeduLabeledPhotoURL',
        'muohioeduMailStopCode',
        'muohioeduMajor',
        'muohioeduMesgAddr',
        'muohioeduMiddleInitial',
        'muohioeduMiddleName',
        'muohioeduMinor',
        'muohioeduPagerEmail',
        'muohioeduPhotoURL',
        'muohioeduPrimaryAffiliation',
        'muohioeduPrimaryLocation',
        'muohioeduProjects',
        'muohioeduRecordInactive',
        'pager',
        'personalTitle',
        'postalAddress',
        'sn',
        'telephoneNumber',
        'title',
        'uid',
    ];

    public static function setShortAttributeList(array $list): void
    {
        self::$shortAttributeList = $list;
    }

    public static function setLongAttributeList(array $list): void
    {
        self::$fullAttributeList = $list;
    }

    public function __construct(LdapManager $ldapRead, LdapManager $ldapWrite = null)
    {
        $this->ldapManagerRead = $ldapRead;
        $this->ldapManagerWrite = $ldapWrite;
    }

    public function findEntryByUniqueId(string $uniqueId): AttributesInterface
    {
        $entry = $this->ldapManagerRead->buildLdapQuery()
            ->select(self::$fullAttributeList)
            ->where(['uid' => $uniqueId])
            ->getLdapQuery()
            ->getSingleResult();

        return new AttributesLdap($entry);
    }

    public function findEntriesByUniqueIdArray(array $idList): AttributesCollection
    {

        $collection = new AttributesCollection([]);

        if (empty($idList)) {
            return $collection;
        }

        foreach (array_chunk($idList, 10) as $batch) {
            $queryBuilder = $this->ldapManagerRead->buildLdapQuery()
                ->select(self::$shortAttributeList);

            foreach ($batch as $id) {
                $queryBuilder->orWhere(['uid' => $id]);
            }

            $users = $queryBuilder->getLdapQuery()->getResult();

            foreach ($users as $user) {
                $collection->addAttributes(new AttributesLdap($user));
            }
        }

        return $collection;
    }

    /**
     * @param $entry AttributesLdap
     * @throws \MiamiOH\Directory\Exception\LdapWriteNotConfiguredException
     */
    public function saveEntry($entry): void
    {
        if (null === $this->ldapManagerWrite) {
            throw new LdapWriteNotConfiguredException();
        }
        /**
         *  Save method is moved to the AttributesLdap class
         */
        $entry->saveWith($this->ldapManagerWrite);
        
    }
}
