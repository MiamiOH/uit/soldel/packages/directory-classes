<?php
namespace MiamiOH\Directory;


use GuzzleHttp\Client;
use MiamiOH\Directory\App\Util\Log;
use MiamiOH\Directory\App\Util\NullLog;

class MailBoxLoaderRestng implements MailBoxLoaderInterface
{

    /**
     * @var Client
     */
    private $client;

    /**
     * @var RestConfiguration
     */
    private $configuration;

    /** @var array  */
    private $allowedDomains;

    /**
     * @var Log
     */
    private $log;

    /**
     * MailBoxLoaderRest constructor.
     * @param Client $client
     * @param RestConfiguration $configuration
     * @param Log|null $log
     */
    public function __construct(Client $client, RestConfiguration $configuration, array $allowedDomains = [], Log $log = null)
    {
        $this->client = $client;
        $this->configuration = $configuration;
        $this->allowedDomains = $allowedDomains;
        $this->log = $log ?? new NullLog();
    }

    public function getMailBoxInfoByUniqueId(string $uniqueId): MailBox
    {
        return new MailBox($uniqueId, []);
    }

    public function saveEntry(MailBox $entry): void
    {
        if ($entry->getDirtyFlag()) {
            $uniqueId = $entry->getUniqueId();

            $token = $this->getWSToken();

            $sendasAddresses = [];

            $response = $this->client->request('GET',
                '/api/googleApps/user/nicknames/v3/' . $uniqueId . '?token=' . $token);
            $nicknames = json_decode($response->getBody()->getContents(),true)['data']['nickname'];

            $response = $this->client->request('GET', '/api/googleApps/emailSettings/v3/'.$uniqueId .'/sendas?token='. $token);

            $listOfEmailSettings = json_decode($response->getBody()->getContents(),true)['data'];

            foreach ($listOfEmailSettings['sendas_addresses'] as $setting) {
                $sendasAddresses[] = $setting['address'];
            }

            $mailBoxValues = $entry->getMailAliases();

            foreach ($mailBoxValues as $mailBoxValue) {

                list($username, $domain) = explode('@', $mailBoxValue);

                if ($username !== $uniqueId && in_array($domain, $this->allowedDomains)) {
                    if (!in_array($username, $nicknames)) {

                       $this->client->request('POST', '/api/googleApps/nickname/v3?token=' . $token, [
                            'body' =>json_encode([
                                'owner' => $uniqueId,
                                'nickname' => $username
                            ]),
                            'headers' => [
                                'content-type' => 'application/json'
                            ]
                        ]);
                    }

                    $preferredName = $entry->getPreferredName();

                    $familyName = $entry->getFamilyName();

                    $numOfAttempts = 3;
                    for ($i = 1; $i <= $numOfAttempts; $i++) {
                        try {
                            if (!in_array($mailBoxValue, $sendasAddresses)) {
                                $this->client->request('POST', '/api/googleApps/emailSettings/v3/' . $uniqueId . '/sendas?token=' . $token, [
                                    'body' => json_encode([
                                        'name' => $preferredName . ' ' . $familyName,
                                        'address' => $username . '@' . $domain,
                                        'replyTo' => $username . '@' . $domain,
                                        'makeDefault' => true
                                    ]),
                                    'headers' => [
                                        'content-type' => 'application/json'
                                    ]
                                ]);
                                break;
                            }
                        } catch (\Exception $e) {
                            $sleepExpo = $i * 1.5;
                            sleep($sleepExpo);
                            $this->log->info('Attempt to add alias ' .  $username . ' failed');
                            continue;
                        }
                            throw new \Exception('max number of attempts reached');
                    }
                }
            }

            foreach ($nicknames as $nickname) {
                if (!in_array($nickname, $mailBoxValues)) {
                    $this->client->request('DELETE', '/api/googleApps/nickname/v3/' . $nickname . '?token=' . $token);
                }
            }
        }
    }

    /*
     * To get the token before the REST request is made.
     */
    public function getWSToken()
    {
        $username = $this->configuration->getUsername();
        $password = $this->configuration->getPassword();

        $response = $this->client->request('POST', '/api/authentication/v1', [
            'body' =>json_encode([
                'username' => $username,
                'password' => $password,
                'type' => 'usernamePassword'
            ]),
            'headers' => [
                'content-type' => 'application/json'
            ]
        ]);

        $data = json_decode($response->getBody(), true);

        return $data['data']['token'];
    }
}
