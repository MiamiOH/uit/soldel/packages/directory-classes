<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/29/17
 * Time: 3:27 PM
 */

// The SDK class entries are autoloaded by composer
include_once __DIR__ . '/../vendor/autoload.php';

// The credentials and configuration can come from whatever source is
// appropriate for your deployment.
$config = parse_ini_file(__DIR__ . '/config.ini', true);

$attributeLoader = new \MiamiOH\Directory\EntryAttributesLoaderYaml(__DIR__ . '/entries');
$pNameLoader = new \MiamiOH\Directory\PreferredNameLoaderYaml(__DIR__ . '/names');
$mailboxLoader = new \MiamiOH\Directory\MailBoxLoaderRest();

$mapper = new \MiamiOH\Directory\EntryAttributesMapper($attributeLoader, $pNameLoader, $mailboxLoader);

$user = $mapper->getEntryByUniqueId('bubba');

print "Got user entry for bubba:\n";
print "First name: " . $user->getGivenName() . "\n";
print "Last name: " . $user->getFamilyName() . "\n";
print "Affiliations: " . print_r($user->getAffiliations(), true) . "\n";
print "Mail: " . $user->getMail() . "\n";
print "Mail aliases: " . print_r($user->getMailAliases(), true) . "\n";

$users = $mapper->getEntryListByUniqueIds(['bubba', 'smithd']);

print "\nUsers\n";
foreach ($users as $user) {
    print $user->getGivenName() . ' ' . $user->getMail() . '(' . $user->getMail() . ")\n";
}
