<?php


namespace MiamiOH\Directory\Tests;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Response;
use MiamiOH\Directory\DirectoryListing;
use MiamiOH\Directory\PersonDirectoryPreferences;
use MiamiOH\Directory\PersonDirectoryPreferencesLoaderRest;
use MiamiOH\Directory\RestConfiguration;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use MiamiOH\Directory\Exception\OutOfBoundsException;

/**
 * Class PersonLoaderRestTest
 * @package MiamiOH\Directory\Tests
 */
class PersonDirectoryPreferencesLoaderRestTest extends TestCase
{
    /**
     * @var PersonDirectoryPreferencesLoaderRest
     */
    private $loader;

    /**
     * @var array
     */
    private $container;
    
    private $baseUrl;
    
    private $username;
    
    private $password;

    /**
     * @var string
     */
    private $uniqueId;
    
    private $token;

    public function setUp(): void
    {
        $this->baseUrl = 'https://example.com';
        $this->username = 'whilesprintdowork';
        $this->password = 'abc123';
        $this->uniqueId = 'doej';
    }
    

    public function testCanBeCreatedWithUserNameAndPassword(): void
    {
        $client = $this->newHttpClientWithResponses([]);
        
        $configuration = new RestConfiguration($this->baseUrl,$this->username,$this->password);
        $personLoaderRest = new PersonDirectoryPreferencesLoaderRest($client,$configuration);
        
        $this->assertInstanceOf(PersonDirectoryPreferencesLoaderRest::class,$personLoaderRest);
    }

    public function testCanGetPersonDirectoryPreferencesByUniqueId(): void
    {
     
        $token = ['1234567890asdfghjkl'];
        
        $data = [
            'givenNamePreferred' => ' John',
            'hideMiddleName' => 'Y'
        ];
            
        $client = $this->newHttpClientWithResponses([
            new Response(200,[],json_encode($token)),
            new Response(200,[],json_encode($data))
        ]);
        
        $loader = $this->newResourceLoader($client);
        
        $test = $loader->getPersonDirectoryPreferencesByUniqueId($this->uniqueId);
        
        $this->assertInstanceOf(PersonDirectoryPreferences::class, $test);

    }

//    public function testExpectsExceptionToSavePersonDirectoryPreferences(): void
//    {
//        $data = new PersonDirectoryPreferences('john', 'N', 'L');
//
//        $client = $this->newHttpClientWithResponses([
//            new Response(200,[],json_encode($data)),
//            new Response(200,[],json_encode($data))
//        ]);
//        
//        $uniqueId = 'doej';
//
//        $loader = $this->newResourceLoader($client);
//        
//        $loader->savePersonDirectoryPreferencesEntry($uniqueId,$data);
//        
//    }
    
    private function newHttpClientWithResponses(array $responses): Client
    {

        $mock = new MockHandler($responses);

        $this->container = [];
        $history = Middleware::history($this->container);

        $handler = HandlerStack::create($mock);
        
        $handler->push($history);

        return new Client(['handler' => $handler]);
    }

    private function newResourceLoader(Client $client): PersonDirectoryPreferencesLoaderRest
    {
        /** @var RestConfiguration|PHPUnit_Framework_MockObject_MockObject $configuration */

        $configuration = $this->createMock(RestConfiguration::class);
        
        /** @var PersonDirectoryPreferencesLoaderRest $loader */
        $loader = new PersonDirectoryPreferencesLoaderRest($client, $configuration);
        
        return $loader;
    }
    
}
