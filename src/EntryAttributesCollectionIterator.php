<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/1/17
 * Time: 11:05 AM
 */

namespace MiamiOH\Directory;


class EntryAttributesCollectionIterator implements \Iterator
{

    private $current = 0;

    /**
     * @var EntryAttributes[]
     */
    private $entries;

    public function __construct(EntryAttributesCollection $entryCollection)
    {
        $this->entries = $entryCollection->toArray();
    }

    public function current(): EntryAttributes
    {
        return $this->entries[$this->current];
    }

    public function next(): void
    {
        $this->current++;
    }

    public function key(): int
    {
        return $this->current;
    }

    public function valid(): bool
    {
        return isset($this->entries[$this->current]);
    }

    public function rewind(): void
    {
        $this->current = 0;
    }
}