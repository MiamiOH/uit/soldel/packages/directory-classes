<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/29/17
 * Time: 10:22 AM
 */

namespace MiamiOH\Directory;


class EntryAttributesLoaderYaml implements EntryAttributesLoaderInterface
{

    /**
     * @var string
     */
    private $sourceDir;

    public function __construct(string $sourceDir)
    {
        $this->sourceDir = $sourceDir;
    }

    public function findEntryByUniqueId(string $uniqueId): AttributesInterface
    {
        return $this->loadEntryFromFile($uniqueId);
    }

    public function findEntriesByUniqueIdArray(array $idList): AttributesCollection
    {
        $entries = [];

        foreach ($idList as $id) {
            $entries[] = $this->loadEntryFromFile($id);
        }

        return new AttributesCollection($entries);
    }

    private function loadEntryFromFile(string $uniqueId): AttributesArray
    {
        $file = implode(DIRECTORY_SEPARATOR, [$this->sourceDir, $uniqueId . '.yml']);

        if (!file_exists($file)) {
            throw new \InvalidArgumentException('Entry source file not found: ' . $file);
        }

        $entry = yaml_parse_file($file);

        return new AttributesArray($entry);
    }

    public function saveEntry($entry): void
    {
        // TODO: Implement saveEntry() method.
    }
}
