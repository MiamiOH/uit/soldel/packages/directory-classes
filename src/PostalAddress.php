<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 9/7/17
 * Time: 9:13 AM
 */

namespace MiamiOH\Directory;


class PostalAddress
{

    /**
     * @var array
     */
    private $address;

    public function __construct(array $address)
    {

        $this->address = $address;
    }

    public function __toString(): string
    {
        return implode("\n", $this->address);
    }

    public function toArray(): array
    {
        return $this->address;
    }

}