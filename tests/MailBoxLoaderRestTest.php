<?php
/**
 * Created by PhpStorm.
 * User: ravendpp
 * Date: 6/29/17
 * Time: 2:07 PM
 */

namespace MiamiOH\Directory\Tests;

use GuzzleHttp\Client;
use MiamiOH\Directory\MailBoxLoaderRest;
use MiamiOH\Directory\MailBox;
use MiamiOH\Directory\RestConfiguration;
use PHPUnit\Framework\TestCase;

class MailBoxLoaderRestTest extends TestCase
{
    /**
     * @var MailBoxLoaderRest
     */
    private $mailBoxLoader;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var RestConfiguration
     */
    private $configuration;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;
    
    /**
     * @var string
     */
    private $baseUrl;
    
    protected function setUp(): void
    {
        $this->client = new Client();
        $this->baseUrl = 'https://example.com';
        $this->username = 'whilesprintdowork';
        $this->password = 'abc123';
        $this->configuration = new RestConfiguration($this->baseUrl, $this->username, $this->password);
        $this->mailBoxLoader = new MailBoxLoaderRest($this->client, $this->configuration);

    }
    public function testCanGetMailBoxFromLoader(): void
    {
        $this->assertInstanceOf(MailBox::class,$this->mailBoxLoader->getMailBoxInfoByUniqueId('bob'));   
    }

}
