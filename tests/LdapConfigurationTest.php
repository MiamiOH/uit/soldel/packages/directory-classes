<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/29/17
 * Time: 2:03 PM
 */

namespace MiamiOH\Directory\Tests;

use MiamiOH\Directory\LdapConfiguration;
use PHPUnit\Framework\TestCase;

class LdapConfigurationTest extends TestCase
{

    /**
     * @var LdapConfiguration
     */
    private $configuration;

    private $domain = '';
    private $url = '';
    private $dn = '';
    private $password = '';
    private $baseDn = '';

    public function setUp(): void
    {
        $this->domain = 'example.com';
        $this->url = 'ldaps://example.com';
        $this->dn = 'cn=bob,dc=example,dc=com';
        $this->password = 'secr3t';
        $this->baseDn = 'ou=people,dc=example,dc=com';

        $this->configuration = new LdapConfiguration($this->domain, $this->url, $this->dn, $this->password, $this->baseDn);
    }

    public function testCanBeCreatedWithValues(): void
    {
        $this->assertInstanceOf(LdapConfiguration::class, $this->configuration);
    }

    public function testCanGetDomain(): void
    {
        $this->assertEquals($this->domain, $this->configuration->getDomain());
    }

    public function testCanGetUrl(): void
    {
        $this->assertEquals($this->url, $this->configuration->getUrl());
    }

    public function testCanGetDn(): void
    {
        $this->assertEquals($this->dn, $this->configuration->getDn());
    }

    public function testCanGetPassword(): void
    {
        $this->assertEquals($this->password, $this->configuration->getPassword());
    }

    public function testCanGetBaseDn(): void
    {
        $this->assertEquals($this->baseDn, $this->configuration->getBaseDn());
    }

}
