<?php
/**
 * Created by PhpStorm.
 * User: ravendpp
 * Date: 6/29/17
 * Time: 2:03 PM
 */

namespace MiamiOH\Directory;


use GuzzleHttp\Client;

class MailBoxLoaderRest implements MailBoxLoaderInterface
{

    /**
     * @var Client
     */
    private $client;

    /**
     * @var RestConfiguration
     */
    private $configuration;

    /**
     * MailBoxLoaderRest constructor.
     * @param Client $client
     * @param RestConfiguration $configuration
     */
    public function __construct(Client $client, RestConfiguration $configuration)
    {
        $this->client = $client;
        $this->configuration = $configuration;
    }

    public function getMailBoxInfoByUniqueId(string $uniqueId): MailBox
    {
        return new MailBox($uniqueId, []);
    }

    public function saveEntry(MailBox $entry): void
    {
        if ($entry->getDirtyFlag()) {
            $uniqueId = $entry->getUniqueId();

            $token = $this->getWSToken();

            $nicknames = [];

            $sendasAddresses = [];

            $response = $this->client->request('GET',
                '/GoogleApps/user/nicknames/' . $uniqueId . '.xml?token=' . $token);

            $xmlResponse = simplexml_load_string($response->getBody());

            foreach ($xmlResponse->nickname as $nickname) {
                $nicknames[] = (string)$nickname;
            }

            $token = $this->getWSToken();

            $response = $this->client->request('GET',
                '/GoogleApps/emailSettings/' . $uniqueId . '/sendas.xml?token=' . $token);

            $xmlResponse = simplexml_load_string($response->getBody());

            foreach ($xmlResponse->sendas as $sendas) {
                $sendasAddresses[] = (string)$sendas->address;
            }

            $mailBoxValues = $entry->getMailAliases();

            foreach ($mailBoxValues as $mailBoxValue) {
                list($username, $domain) = explode('@', $mailBoxValue);

                if ($username !== $uniqueId && in_array($domain, ['miamioh.edu', 'muohio.edu'])) {
                    if (!in_array($username, $nicknames)) {

                        $body = 'owner=' . $uniqueId . '&nickname=' . $username;

                        $this->client->request('POST', '/GoogleApps/nickname/?token=' . $token, [
                            'body' => $body,
                            'headers' => [
                                'Content-Type' => 'application/x-www-form-urlencoded'
                            ]
                        ]);
                    }

                    $preferredName = $entry->getPreferredName();

                    $familyName = $entry->getFamilyName();

                    if (!in_array($mailBoxValue, $sendasAddresses)) {
                        $body = 'name=' . $preferredName . ' ' . $familyName .
                            '&address=' . $username . '@' . $domain .
                            '&replyTo=' . $username . '@' . $domain .
                            '&makeDefault=true';

                        $this->client->request('POST',
                            '/GoogleApps/emailSettings/' . $uniqueId . '/sendas.xml?token=' . $token, [
                                'body' => $body,
                                'headers' => [
                                    'Content-Type' => 'application/x-www-form-urlencoded'
                                ]
                            ]);
                    }
                }
            }

            foreach ($nicknames as $nickname) {
                if (!in_array($nickname, $mailBoxValues)) {
                    $this->client->request('DELETE',
                        '/GoogleApps/nickname/' . $nickname . '.xml?token=' . $token);
                }
            }
        }
    }

    /*
     * To get the token before the REST request is made.
     */
    public function getWSToken()
    {
        $username = $this->configuration->getUsername();
        $password = $this->configuration->getPassword();

        $response = $this->client->request('POST', '/authentication.json', [
            'form_params' => [
                'application' => 'preferredName',
                'type' => 'usernamePassword',
                'username' => $username,
                'password' => $password
            ]
        ]);

        $data = json_decode($response->getBody(), true);

        return $data['token'];
    }
}