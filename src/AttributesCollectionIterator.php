<?php
/**
 * Created by PhpStorm.
 * User: shettycm
 * Date: 6/29/17
 * Time: 3:26 PM
 */

namespace MiamiOH\Directory;


class AttributesCollectionIterator implements \Iterator
{
    private $current = 0;

    /**
     * @var AttributesInterface[]
     */
    private $attributes;

    public function __construct(AttributesCollection $attributesCollection)
    {
        $this->attributes = $attributesCollection->toArray();
    }

    public function current(): AttributesInterface
    {
        return $this->attributes[$this->current];
    }

    public function next(): void
    {
        $this->current++;
    }

    public function key(): int
    {
        return $this->current;
    }

    public function valid(): bool
    {
        return isset($this->attributes[$this->current]);
    }

    public function rewind(): void
    {
        $this->current = 0;
    }

}