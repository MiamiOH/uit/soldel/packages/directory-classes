<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/1/17
 * Time: 11:02 AM
 */

namespace MiamiOH\Directory;


use MiamiOH\Directory\Exception\EntryNotFoundException;

class EntryAttributesCollection implements \Countable, \IteratorAggregate
{

    /**
     * @var EntryAttributes[]
     */
    private $entries;

    public function __construct(array $entries)
    {
        $this->ensure($entries);
        $this->entries = $entries;
    }

    private function ensure(array $entries): void
    {
        foreach ($entries as $entry) {
            if (!$entry instanceof EntryAttributes) {
                throw new \InvalidArgumentException();
            }
        }
    }

    public function getIterator(): EntryAttributesCollectionIterator
    {
        return new EntryAttributesCollectionIterator($this);
    }

    public function count(): int
    {
        return count($this->entries);
    }

    public function toArray(): array
    {
        return $this->entries;
    }
    
    public function getByUniqueId(string $uniqueId): EntryAttributes
    {
        foreach ($this->entries as $entry) {
            if ($entry->getUniqueId() === $uniqueId) {
                return $entry;
            }
        }

        throw new EntryNotFoundException();
    }
}