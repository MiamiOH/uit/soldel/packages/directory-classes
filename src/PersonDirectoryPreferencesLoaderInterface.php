<?php

namespace MiamiOH\Directory;


interface PersonDirectoryPreferencesLoaderInterface
{
    public function getPersonDirectoryPreferencesByUniqueId(string $uniqueId): PersonDirectoryPreferences;
    public function loadPreferences(string $uniqueId): array;
    public function savePersonDirectoryPreferencesEntry(PersonDirectoryPreferences $entry): void;
    public function checkEmailAliases(array $aliases): array;
    public function recordEmailAliases(string $uniqueId, array $aliasData): void;

}
