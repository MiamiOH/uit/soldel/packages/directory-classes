<?php
/**
 * Created by PhpStorm.
 * User: shettycm
 * Date: 6/29/17
 * Time: 2:52 PM
 */

namespace MiamiOH\Directory\Tests;


use LdapTools\Object\LdapObject;
use MiamiOH\Directory\AttributesLdap;
use MiamiOH\Directory\AttributesCollection;
use PHPUnit\Framework\TestCase;

class AttributesCollectionTest extends TestCase
{
    /**
     * @var AttributesCollection
     */
    private $collection;

    public function setUp(): void
    {
        $ldapEntry1 = new LdapObject();
        $ldapEntry1->set('mail', 'john@miamioh.edu');
        $ldapEntry1->set('givenName', 'John');
        $ldapEntry1->set('sn', 'Doe');
        $ldapEntry1->set('middlename', '');

        $ldapEntry2 = new LdapObject();
        $ldapEntry2->set('mail', 'smith@miamioh.edu');
        $ldapEntry2->set('givenName', 'Dan');
        $ldapEntry2->set('sn', 'Smith');
        $ldapEntry2->set('middlename', '');

        $attributes = [
            new AttributesLdap($ldapEntry1),
            new AttributesLdap($ldapEntry2)
        ];
        $this->collection = new AttributesCollection($attributes);
    }

    public function testCanBeCreated(): void
    {
        $this->assertInstanceOf(AttributesCollection::class, $this->collection);
    }

    public function testCanBeCounted(): void
    {
        $this->assertCount(2, $this->collection);
    }

    public function testCanBeUsedAsArray(): void
    {
        $data = $this->collection->toArray();
        $this->assertInstanceOf(AttributesLdap::class, $data[0]);
    }

    public function testCanBeIteratedAsArray(): void
    {
        foreach ($this->collection as $attributes) {
            $this->assertInstanceOf(AttributesLdap::class, $attributes);
        }
    }


}