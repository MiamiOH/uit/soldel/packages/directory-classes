<?php


namespace MiamiOH\Directory;


use GuzzleHttp\Client;


class PersonDirectoryPreferencesLoaderRest implements PersonDirectoryPreferencesLoaderInterface
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var RestConfiguration
     */
    private $configuration;

    public function __construct(Client $client, RestConfiguration $configuration)
    {
        $this->client = $client;
        $this->configuration = $configuration;

    }

    public function getWSToken()
    {
        $username = $this->configuration->getUsername();
        $password = $this->configuration->getPassword();

        $response = $this->client->request('POST', '/api/authentication/v1', [
            'body' =>json_encode([
                'username' => $username,
                'password' => $password,
                'type' => 'usernamePassword'
            ]),
            'headers' => [
                'content-type' => 'application/json'
            ]
        ]);

        $data = json_decode($response->getBody(), true)['data'];

        return $data['token'];
    }

    public function getPersonDirectoryPreferencesByUniqueId(string $uniqueId): PersonDirectoryPreferences
    {
        return new PersonDirectoryPreferences($uniqueId, $this);
    }

    public function loadPreferences(string $uniqueId): array
    {
        $preferences = [];
        $token = $this->getWSToken();

        $response = $this->client->request('GET', 'api/person/v2?uniqueId=' . $uniqueId . '&token=' . $token,
            ['headers' => [
                'content-type' => 'application/json'
            ]
        ]);

        $data = json_decode($response->getBody(), true)['data'];

        if (!is_null($data[0]['givenNamePreferred'])) {
            $preferences['givenNamePreferred'] = $data[0]['givenNamePreferred'];
            $preferences['hideMiddleName'] = $data[0]['hideMiddleName'];
            $preferences['directoryListing'] = $data[0]['directoryListing'];
        }

        return $preferences;
        
    }

    public function savePersonDirectoryPreferencesEntry(PersonDirectoryPreferences $entry): void
    {
        /**
         *  To check the dirty flag whether there is any change in Person Directory Preferences attributes and execute the REST PUT request.
         *  dirtyFlag = true
         */
        if ($entry->getDirtyFlag()) {

            $token = $this->getWSToken();

            $formParams = [];

            $uniqueId = $entry->getUniqueId();

            $directoryListing = $entry->getDirectoryListing();
            $preferredName = $entry->getPreferredName();

            if (null !== $preferredName) {
                $formParams['preferredFirstName'] = $preferredName;
            }

            if ($directoryListing !== '') {
                $formParams['directoryListing'] = $directoryListing;
            }

                $formParams['hideMiddleName'] = $entry->getHideMiddleName() ? 'true' : 'false';

            $body = implode('&', array_map(
                function ($key, $value) {
                    return implode('=',[$key, $value]);
                },
                array_keys($formParams),
                array_values($formParams)
            ));

            $this->client->request('PUT', 'api/person/v3?uniqueId=' . $uniqueId . '&token=' . $token . '&' . $body, [
                'headers' => [
                    'content-type' => 'application/json'
                ]
            ]);

        }
    }

    public function checkEmailAliases(array $aliases): array
    {
        $response = $this->client->request(
            'GET',
            'api/directory/v1/alias?alias=' . implode(',', $aliases) .
            '&token=' . $this->getWSToken(),
                ['headers' => [
                    'content-type' => 'application/json'
                ]
            ]
        );

        return json_decode($response->getBody(), true)['data'];
    }

    public function recordEmailAliases(string $uniqueId, array $aliasData): void
    {
        $this->client->request(
            'POST',
            'api/directory/v1/alias/' . $uniqueId . '?token=' . $this->getWSToken(),
            ['body' => json_encode($aliasData),
                'headers' => [
                    'content-type' => 'application/json'
                ]
            ]
        );
    }
}
