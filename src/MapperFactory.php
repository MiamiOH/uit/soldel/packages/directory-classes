<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/29/17
 * Time: 2:30 PM
 */

namespace MiamiOH\Directory;


use GuzzleHttp\Client;
use LdapTools\Configuration;
use LdapTools\DomainConfiguration;
use LdapTools\LdapManager;

class MapperFactory
{

    public static function getLdapManager(LdapConfiguration $configuration): LdapManager
    {
        $config = new Configuration();

        $domain = (new DomainConfiguration($configuration->getDomain()))
            ->setUseSsl(true)
            ->setPort(636)
            ->setBaseDn($configuration->getBaseDn())
            ->setServers([$configuration->getUrl()])
            ->setUsername($configuration->getDn())
            ->setPassword($configuration->getPassword()
        );

        $config->addDomain($domain);

        return new LdapManager($config);
    }

    public static function getHttpClient(RestConfiguration $configuration): Client
    {
        $baseUrl = $configuration->getBaseUrl();
        return new Client(['base_uri' => $baseUrl]);
    }

}