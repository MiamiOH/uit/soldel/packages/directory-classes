<?php

namespace MiamiOH\Directory\App\Util;

use Psr\Log\LoggerInterface;

interface Log extends LoggerInterface
{
}