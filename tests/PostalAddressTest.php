<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 9/7/17
 * Time: 9:16 AM
 */

namespace MiamiOH\Directory\Tests;

use MiamiOH\Directory\PostalAddress;
use PHPUnit\Framework\TestCase;

class PostalAddressTest extends TestCase
{

    private $addressArray = [];

    /**
     * @var PostalAddress
     */
    private $address;

    public function setUp(): void
    {
        $this->addressArray = [
            '123 Main St.',
            'Anytown, OH 45000'
        ];

        $this->address = new PostalAddress($this->addressArray);

    }
    public function testCanBeCreatedFromArray(): void
    {
        $this->assertInstanceOf(PostalAddress::class, $this->address);
    }

    public function testCanBeUsedAsString(): void
    {
        $this->assertEquals(implode("\n", $this->addressArray), (string) $this->address);
    }

    public function testCanBeUsedAsArray(): void
    {
        $this->assertEquals(
            implode("\n", $this->addressArray),
            implode("\n", $this->address->toArray())
        );
    }

}
