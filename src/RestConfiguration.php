<?php
/**
 * Created by PhpStorm.
 * User: shettycm
 * Date: 6/30/17
 * Time: 9:12 AM
 */

namespace MiamiOH\Directory;


class RestConfiguration
{
    /**
     * @var string
     */
    private $baseUrl;
    /**
     * @var string
     */
    private $username;
    /**
     * @var string
     */
    private $password;

    public function __construct(string $baseUrl, string $username='', string $password='')
    {
        $this->baseUrl = $baseUrl;
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getBaseUrl(): string
    {
        return $this->baseUrl;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }
}