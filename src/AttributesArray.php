<?php
/**
 * Created by PhpStorm.
 * User: shettycm
 * Date: 6/29/17
 * Time: 2:38 PM
 */

namespace MiamiOH\Directory;


use MiamiOH\Directory\Exception\AttributeUpdateNotImplementedException;

class AttributesArray implements AttributesInterface
{

    /**
     * @var array
     */
    private $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    private function getAttribute(string $attribute, $default = '')
    {
        // TODO use default type (string or array) to ensure correct return type
        if (array_key_exists($attribute, $this->data)) {
            $value = $this->data[$attribute];
            if (is_array($default) && !is_array($value)) {
                return [$value];
            } elseif (!is_array($default) && is_array($value)) {
                return $value[0];
            }

            return $value;
        }

        return $default;
    }

    public function getBuildingNames(): array
    {
        return $this->getAttribute('buildingName', []);
    }

    public function setBuildingNames(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getFaxNumber(): string
    {
        return $this->getAttribute('facsimileTelephoneNumber');
    }

    public function setFaxNumber(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getNameSuffix(): string
    {
        return $this->getAttribute('generationQualifier');
    }

    public function setNameSuffix(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getGivenName(): string
    {
        return $this->getAttribute('givenName');
    }

    public function setGivenName(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getHomePhone(): array
    {
        return $this->getAttribute('homePhone', []);
    }

    public function setHomePhone(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getHomePostalAddress(): PostalAddress
    {
        return new PostalAddress($this->getAttribute('homePostalAddress', []));
    }

    public function setHomePostalAddress(PostalAddress $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getLocations(): array
    {
        return $this->getAttribute('l', []);
    }

    public function setLocations(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getLabeledUri(): string
    {
        return $this->getAttribute('labeledUri');
    }

    public function setLabeledUri(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getMail(): string
    {
        return $this->getAttribute('mail');
    }

    public function setMail(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getMailAliases(): array
    {
        return $this->getAttribute('mailAlternateAddress', []);
    }

    public function setMailAliases(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getMailRoutingAddress(): string
    {
        return $this->getAttribute('mailRoutingAddress');
    }

    public function setMailRoutingAddress(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getMobileNumber(): string
    {
        return $this->getAttribute('mobile');
    }

    public function setMobileNumber(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getAdditionalAffiliations(): array
    {
        return $this->getAttribute('muohioeduAdditionalAffiliations', []);
    }

    public function setAdditionalAffiliations(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getAffiliations(): array
    {
        return $this->getAttribute('muohioeduAffiliation', []);
    }

    public function setAffiliations(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getAffiliationCodes(): array
    {
        return $this->getAttribute('muohioeduAffiliationCode', []);
    }

    public function setAffiliationCodes(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getBuildingCodes(): array
    {
        return $this->getAttribute('muohioeduBuildingCode', []);
    }

    public function setBuildingCodes(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getCampusTelephoneNumber(): string
    {
        return $this->getAttribute('muohioeduCampusTelephoneNumber');
    }

    public function setCampusTelephoneNumber(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getClasses(): array
    {
        return $this->getAttribute('muohioeduClass', []);
    }

    public function setClasses(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getClassCodes(): array
    {
        return $this->getAttribute('muohioeduClassCode', []);
    }

    public function setClassCodes(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getColleges(): array
    {
        return $this->getAttribute('muohioeduCollege', []);
    }

    public function setColleges(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getCollegeCodes(): array
    {
        return $this->getAttribute('muohioeduCollegeCode', []);
    }

    public function setCollegeCodes(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getDepartments(): array
    {
        return $this->getAttribute('muohioeduDepartment', []);
    }

    public function setDepartments(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getDepartmentCodes(): array
    {
        return $this->getAttribute('muohioeduDepartmentCode', []);
    }

    public function setDepartmentCodes(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getDirectoryProxies(): array
    {
        return $this->getAttribute('muohioeduDirectoryProxy', []);
    }

    public function setDirectoryProxies(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getDivisions(): array
    {
        return $this->getAttribute('muohioeduDivision', []);
    }

    public function setDivisions(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getDivisionCodes(): array
    {
        return $this->getAttribute('muohioeduDivisionCode', []);
    }

    public function setDivisionCodes(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getEmployeeSchools(): array
    {
        return $this->getAttribute('muohioeduEmployeeSchool', []);
    }

    public function setEmployeeSchools(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getEmployeeSchoolCodes(): array
    {
        return $this->getAttribute('muohioeduEmployeeSchoolCode', []);
    }

    public function setEmployeeSchoolCodes(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getEmployeeType(): string
    {
        return $this->getAttribute('muohioeduEmployeeType');
    }

    public function setEmployeeType(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getEmployeeTypeCode(): string
    {
        return $this->getAttribute('muohioeduEmployeeTypeCode');
    }

    public function setEmployeeTypeCode(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getGreekOrg(): string
    {
        return $this->getAttribute('muohioeduGreekOrg');
    }

    public function setGreekOrg(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getHidden(): string
    {
        return $this->getAttribute('muohioeduHidden');
    }

    public function setHidden(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getHigherEducation(): string
    {
        return $this->getAttribute('muohioeduHigherEducation');
    }

    public function setHigherEducation(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getHighschoolAttended(): string
    {
        return $this->getAttribute('muohioeduHighschoolAttended');
    }

    public function setHighschoolAttended(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getHours(): string
    {
        return $this->getAttribute('muohioeduHours');
    }

    public function setHours(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getLabeledPhotoURL(): string
    {
        return $this->getAttribute('muohioeduLabeledPhotoURL');
    }

    public function setLabeledPhotoURL(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getLocationCodes(): array
    {
        return $this->getAttribute('muohioeduLocationCode', []);
    }

    public function setLocationCodes(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getMailStopCodes(): array
    {
        return $this->getAttribute('muohioeduMailStopCode', []);
    }

    public function setMailStopCodes(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getMajors(): array
    {
        return $this->getAttribute('muohioeduMajor', []);
    }

    public function setMajors(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getMajorCodes(): array
    {
        return $this->getAttribute('muohioeduMajorCode', []);
    }

    public function setMajorCodes(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getMesgAddr(): string
    {
        return $this->getAttribute('muohioeduMesgAddr');
    }

    public function setMesgAddr(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getMiddleInitial(): string
    {
        return $this->getAttribute('muohioeduMiddleInitial');
    }

    public function setMiddleInitial(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getMiddleName(): string
    {
        return $this->getAttribute('muohioeduMiddleName');
    }

    public function setMiddleName(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getMinors(): array
    {
        return $this->getAttribute('muohioeduMinor', []);
    }

    public function setMinors(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getMinorCodes(): array
    {
        return $this->getAttribute('muohioeduMinorCode', []);
    }

    public function setMinorCodes(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getPagerEmail(): string
    {
        return $this->getAttribute('muohioeduPagerEmail');
    }

    public function setPagerEmail(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getPhotoURL(): string
    {
        return $this->getAttribute('muohioeduPhotoURL');
    }

    public function setPhotoURL(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getPrimaryAffiliation(): string
    {
        return $this->getAttribute('muohioeduPrimaryAffiliation');
    }

    public function setPrimaryAffiliation(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getPrimaryAffiliationCode(): string
    {
        return $this->getAttribute('muohioeduPrimaryAffiliationCode');
    }

    public function setPrimaryAffiliationCode(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getPrimaryLocation(): string
    {
        return $this->getAttribute('muohioeduPrimaryLocation');
    }

    public function setPrimaryLocation(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getPrimaryLocationCode(): string
    {
        return $this->getAttribute('muohioeduPrimaryLocationCode');
    }

    public function setPrimaryLocationCode(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getProjects(): string
    {
        return $this->getAttribute('muohioeduProjects');
    }

    public function setProjects(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getRecordInactive(): string
    {
        return $this->getAttribute('muohioeduRecordInactive');
    }

    public function setRecordInactive(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getVPNGroup(): string
    {
        return $this->getAttribute('muohioeduVPNGroup');
    }

    public function setVPNGroup(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getPagerNumber(): string
    {
        return $this->getAttribute('pager');
    }

    public function setPagerNumber(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getNamePrefix(): string
    {
        return $this->getAttribute('personalTitle');
    }

    public function setNamePrefix(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getPostalAddress(): PostalAddress
    {
        return new PostalAddress($this->getAttribute('postalAddress', []));
    }

    public function setPostalAddress(PostalAddress $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getFamilyName(): string
    {
        return $this->getAttribute('sn');
    }

    public function setFamilyName(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getTelephoneNumber(): array
    {
        return $this->getAttribute('telephoneNumber', []);
    }

    public function setTelephoneNumber(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getTitles(): array
    {
        return $this->getAttribute('title', []);
    }

    public function setTitles(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getUniqueId(): string
    {
        return $this->getAttribute('uid');
    }

    public function setUniqueId(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getDirectoryListing(): string
    {
        return $this->getAttribute('muohioeduDirectoryListing');
    }

    public function setDirectoryListing(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getDisplayName(): string
    {
        return $this->getAttribute('displayName');
    }

    public function setDisplayName(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getEntitlements(): array
    {
        return $this->getAttribute('eduPersonEntitlements', []);
    }

    public function setEntitlements(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }
}

