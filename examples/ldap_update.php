<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/29/17
 * Time: 3:27 PM
 */

// The SDK class entries are autoloaded by composer
include_once __DIR__ . '/../vendor/autoload.php';

// The credentials and configuration can come from whatever source is
// appropriate for your deployment.
$config = parse_ini_file(__DIR__ . '/config.ini', true);

$ldap = \MiamiOH\Directory\MapperFactory::getLdapManager(new \MiamiOH\Directory\LdapConfiguration(
    $config['LDAP']['domain'],
    $config['LDAP']['url'],
    $config['LDAP']['dn'],
    $config['LDAP']['pw'],
    $config['LDAP']['basedn']
));

$ldapWrite = \MiamiOH\Directory\MapperFactory::getLdapManager(new \MiamiOH\Directory\LdapConfiguration(
    $config['LDAP_WRITE']['domain'],
    $config['LDAP_WRITE']['url'],
    $config['LDAP_WRITE']['dn'],
    $config['LDAP_WRITE']['pw'],
    $config['LDAP_WRITE']['basedn']
));

$ldapLoader = new \MiamiOH\Directory\EntryAttributesLoaderLdap($ldap, $ldapWrite);
$pNameLoader = new \MiamiOH\Directory\PreferredNameLoaderYaml(__DIR__ . '/names');
$mailboxLoader = new \MiamiOH\Directory\MailBoxLoaderRest();

$mapper = new \MiamiOH\Directory\EntryAttributesMapper($ldapLoader, $pNameLoader, $mailboxLoader);

// $user will be an EntryAttributes object
$user = $mapper->getEntryByUniqueId('tepeds');

$project = 'This is my project as of ' . date('Y-m-d H:m:s');
print "Update project to $project\n";
$user->setProjects($project);

if ($user->getHigherEducation()) {
    print "Clearing higher ed\n";
    $user->setHigherEducation('');
} else {
    $higherEd = 'Some college';
    print "Setting higher ed to $higherEd\n";
    $user->setHigherEducation($higherEd);
}

$phone = $user->getTelephoneNumber();
print "set phone to $phone\n";
$user->setTelephoneNumber($phone);

print "set high school to empty\n";
$user->setHighschoolAttended('');

$proxies = $user->getDirectoryProxies();

if (!empty($proxies)) {
    $removeProxy = array_shift($proxies);
    print "Remove proxy $removeProxy\n";
}

while (count($proxies) < 3) {
    $newProxy = 'uid=' . uniqid('proxy_', true) . ',ou=people,dc=muohio,dc=edu';
    print "Add proxy $newProxy\n";
    $proxies[] = $newProxy;
}

$user->setDirectoryProxies($proxies);

$mapper->saveEntry($user);

print "saved\n";