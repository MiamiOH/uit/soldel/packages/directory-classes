<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/29/17
 * Time: 10:34 AM
 */

namespace MiamiOH\Directory\Tests;

use GuzzleHttp\Client;
use LdapTools\LdapManager;
use MiamiOH\Directory\EntryAttributes;
use MiamiOH\Directory\EntryAttributesLoaderLdap;
use MiamiOH\Directory\EntryAttributesMapper;
use MiamiOH\Directory\MailBoxLoaderRest;
use MiamiOH\Directory\LdapConfiguration;
use MiamiOH\Directory\PersonDirectoryPreferencesLoaderRest;
use MiamiOH\Directory\PreferredNameLoaderRest;
use MiamiOH\Directory\RestConfiguration;
use PHPUnit\Framework\TestCase;

class EntryAttributesMapperTest extends TestCase
{
    /**
     * @var LdapConfiguration
     */
    private $ldapConfiguration;
    /**
     * @var LdapManager
     */
    private $ldapManager;
    /**
     * @var Client
     */
    private $client;

    /**
     * @var RestConfiguration
     */
    private $configuration;

    public function setUp(): void
    {
        $this->ldapManager = $this->createMock(LdapManager::class);
        $this->client = $this->createMock(Client::class);
        $this->configuration = $this->createMock(RestConfiguration::class);
    }
    public function testCanUseLoader(): void
    {
        $mapper = new EntryAttributesMapper(
            new EntryAttributesLoaderLdap($this->ldapManager),
            new PersonDirectoryPreferencesLoaderRest($this->client, $this->configuration),
            new MailBoxLoaderRest($this->client, $this->configuration)
        );

        $this->assertInstanceOf(EntryAttributesMapper::class,
            $mapper);

    }
}
