<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/29/17
 * Time: 10:21 AM
 */

namespace MiamiOH\Directory;


interface EntryAttributesLoaderInterface
{
    public function findEntryByUniqueId(string $uniqueId): AttributesInterface;
    public function findEntriesByUniqueIdArray(array $idList): AttributesCollection;
    public function saveEntry($entry): void;
}