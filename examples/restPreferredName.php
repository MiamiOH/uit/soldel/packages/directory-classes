<?php

/**
 * Created by PhpStorm.
 * User: shettycm
 * Date: 6/30/17
 * Time: 10:22 AM
 */

include_once __DIR__ . '/../vendor/autoload.php';

// The credentials and configuration can come from whatever source is
// appropriate for your deployment.
$config = parse_ini_file(__DIR__ . '/config.ini', true);

$client = \MiamiOH\Directory\MapperFactory::getHttpClient(new \MiamiOH\Directory\RestConfiguration(
    $config['REST']['person_baseurl']
));

//$loader = new \MiamiOH\Directory\PreferredNameLoaderRest($client);

$configuration = new \MiamiOH\Directory\RestConfiguration($config['REST']['person_baseurl'],$config['REST']['username'],$config['REST']['password']);
$loader = new \MiamiOH\Directory\PersonDirectoryPreferencesLoaderRest($client,$configuration);

$preferences = $loader->getPersonDirectoryPreferencesByUniqueId('tepeds');

print "preferred name: " . $preferences->getPreferredName() . "\n";
print "hide middle name: " . $preferences->getHideMiddleName() . "\n";