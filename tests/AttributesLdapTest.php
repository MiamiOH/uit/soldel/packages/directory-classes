<?php
/**
 * Created by PhpStorm.
 * User: shettycm
 * Date: 6/29/17
 * Time: 2:42 PM
 */

namespace MiamiOH\Directory\Tests;


use LdapTools\Object\LdapObject;
use MiamiOH\Directory\AttributesLdap;
use PHPUnit\Framework\TestCase;


class AttributesLdapTest extends TestCase
{
    /**
     * @var AttributesLdap
     */
    private $attributes;

    private $defaultValues = [
        'mail' => 'john@miamioh.edu',
        'givenName' => 'John',
        'sn' => 'Doe',
    ];
    
    public function setup(): void
    {
        $this->attributes = new AttributesLdap($this->getTestLdapObject($this->defaultValues));
    }
    public function testCanBeCreated(): void
    {
        $this->assertInstanceOf(AttributesLdap::class, $this->attributes);
    }

    public function testCanGetFirstName(): void
    {
        $this->assertEquals('John', $this->attributes->getGivenName());
    }

    public function testCanGetFamilyName(): void
    {
        $this->assertEquals('Doe', $this->attributes->getFamilyName());
    }

    public function testCanGetMiddleName(): void
    {
        $this->assertEquals('', $this->attributes->getMiddleName());
    }

    public function testCanGetMail(): void
    {
        $this->assertEquals('john@miamioh.edu', $this->attributes->getMail());
    }

    private function getTestLdapObject(array $values = []): LdapObject
    {
        $ldapObject = new LdapObject();

        foreach ($values as $attrName => $value) {
            $ldapObject->set($attrName, $value);
        }

        return $ldapObject;
    }
}
