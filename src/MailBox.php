<?php


namespace MiamiOH\Directory;


class MailBox
{
    /**
     * @var array
     */
    private $mailBoxInfo;

    /**
     * @var string
     */
    private $uniqueId;

    /**
     * @var array
     */
    private $mailAliases;

    /**
     * @var string
     */
    private $preferredName;

    /**
     * @var string
     */
    private $familyName;

    /**
     * @var bool
     */
    private $dirtyFlag = false;
    
    public function __construct(string $uniqueId, array $mailBoxInfo)
    {
        $this->mailBoxInfo = $mailBoxInfo;
        $this->uniqueId = $uniqueId;
    }

//    public function getMailBox(): array 
//    {
//        return $this->mailBoxInfo;
//    }

    /**
     * @return array
     */
    public function getMailAliases(): array 
    {   
        return $this->mailAliases;
    }

    /**
     * @param array $mailAliases
     */
    public function setMailAliases(array $mailAliases): void
    {
        unset($mailAliases['preferredName']);
        unset($mailAliases['familyName']);
        $this->mailAliases = $mailAliases;
        $this->dirtyFlag = true;
    }

    /**
     * @return string
     */
    public function getPreferredName(): string 
    {
        return $this->preferredName;
    }

    /**
     * @param string $preferredName
     */
    public function setPreferredName(string $preferredName): void
    {
        $this->preferredName = $preferredName;
    }

    /**
     * @param string $familyName
     */
    public function setFamilyName(string $familyName): void
    {
        $this->familyName = $familyName;
    }

    /**
     * @return string
     */
    public function getFamilyName(): string
    {
        return $this->familyName;
    }

    /**
     * @return string
     */
    public function getUniqueId(): string 
    {
        return $this->uniqueId;
    }

    /**
     * @return bool
     */
    public function getDirtyFlag(): bool
    {
        return $this->dirtyFlag;
    }
}