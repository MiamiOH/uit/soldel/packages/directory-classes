<?php
/**
 * Created by PhpStorm.
 * User: shettycm
 * Date: 6/29/17
 * Time: 2:07 PM
 */

namespace MiamiOH\Directory\Tests;


use LdapTools\Object\LdapObject;
use MiamiOH\Directory\AttributesLdap;
use MiamiOH\Directory\EntryAttributes;
use MiamiOH\Directory\MailBox;
use MiamiOH\Directory\PersonDirectoryPreferences;
use MiamiOH\Directory\PreferredName;
use PHPUnit\Framework\TestCase;


class EntryAttributesTest extends TestCase
{
    /**
     * @var EntryAttributes
     */
    private $entryAttributes;

    public function setup(): void
    {
        $ldapEntry = new LdapObject();
        $ldapEntry->set('mail', 'john@miamioh.edu');
        $ldapEntry->set('givenName', 'John');
        $ldapEntry->set('sn', 'Doe');
        $ldapEntry->set('middlename', '');

        $preferences = $this->getMockBuilder(PersonDirectoryPreferences::class)
            ->setMethods(['getPreferredName'])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $preferences->method('getPreferredName')->willReturn('john');

        $this->entryAttributes = new EntryAttributes(
            new AttributesLdap($ldapEntry),
            $preferences,
            new MailBox('doej',['address','routingType'])
        );
    }
    public function testCanBeCreated(): void
    {
        $this->assertInstanceOf(EntryAttributes::class, $this->entryAttributes);
    }

    public function testCanGetPreferredName(): void
    {
        $this->assertEquals('john', $this->entryAttributes->getPreferredName());
    }

    public function testCanGetGivenName(): void
    {
        $this->assertEquals('John', $this->entryAttributes->getGivenName());
    }

    public function testCanGetFamilyName(): void
    {
        $this->assertEquals('Doe', $this->entryAttributes->getFamilyName());
    }

    public function testCanGetMail(): void
    {
        $this->assertEquals('john@miamioh.edu', $this->entryAttributes->getMail());
    }

    /**
     * Mail box GET is not implemented, as mail aliases data is pulled from LDAP.
     */
//    public function testCanGetMailBoxInfo(): void
//    {
//        $this->assertEquals(['address','routingType'], $this->entryAttributes->getMailBoxInfo());
//    }
}
