<?php

namespace MiamiOH\Directory;


class PersonDirectoryPreferences
{

    /**
     * @var bool
     */
    private $loaded = false;

    /**
     * @var string
     */
    private $uniqueId;
    /**
     * @var PersonDirectoryPreferencesLoaderInterface
     */
    private $loader;

    /**
     * @var string
     */
    private $preferredName;

    /**
     * @var string
     */
    private $directoryListing;

    /**
     * @var bool
     */
    private $hideMiddleName;

    /**
     * @var bool
     */
    private $dirtyFlag = false;

    /**
     * PersonDirectoryPreferences constructor.
     * @param string $uniqueId
     * @param PersonDirectoryPreferencesLoaderInterface $loader
     */
    public function __construct(string $uniqueId, PersonDirectoryPreferencesLoaderInterface $loader)
    {
        $this->uniqueId = $uniqueId;
        $this->loader = $loader;
    }

    private function loadData(): void
    {
        if ($this->loaded || null == $this->loader) {
            return;
        }

        $prefs = $this->loader->loadPreferences($this->uniqueId);

        if (null !== $prefs['givenNamePreferred']) {
            $this->preferredName = $prefs['givenNamePreferred'];
        }

        if (null !== $prefs['hideMiddleName']) {
            $this->hideMiddleName = $prefs['hideMiddleName'];
        }

        if (null !== $prefs['directoryListing']) {
            $this->directoryListing = $prefs['directoryListing'];
        }

        $this->loaded = true;
    }

    public function getUniqueId(): string
    {
        $this->loadData();
        return $this->uniqueId;
    }

    public function getPreferredName(): string
    {
        $this->loadData();

        if(null === $this->preferredName) {
            return '';
        }
        
        return $this->preferredName;
    }

    public function setPreferredName(string $preferredName): void
    {
        $this->loadData();
        $this->preferredName = $preferredName;
        $this->dirtyFlag = true;
    }

    public function setDirectoryListing(string $directoryListing): void
    {
        $this->loadData();
        $this->directoryListing = $directoryListing;
        $this->dirtyFlag = true;
    }

    public function getDirectoryListing(): string
    {
        $this->loadData();
        return $this->directoryListing;
    }

    public function getHideMiddleName(): bool
    {
        $this->loadData();
        
        if(null === $this->hideMiddleName) {
            return false;
        }

        return $this->hideMiddleName;
    }

    public function setHideMiddleName(bool $hideMiddleName): void
    {
        $this->loadData();
        $this->hideMiddleName = $hideMiddleName;
        $this->dirtyFlag = true;
    }

    public function getDirtyFlag(): bool
    {
        return $this->dirtyFlag;
    }
}
