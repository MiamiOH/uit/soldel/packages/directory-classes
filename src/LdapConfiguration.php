<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/29/17
 * Time: 2:02 PM
 */

namespace MiamiOH\Directory;


class LdapConfiguration
{

    /**
     * @var string
     */
    private $domain;
    /**
     * @var string
     */
    private $url;
    /**
     * @var string
     */
    private $dn;
    /**
     * @var string
     */
    private $password;
    /**
     * @var string
     */
    private $baseDn;

    public function __construct(string $domain, string $url, string $dn, string $password, string $baseDn)
    {

        $this->domain = $domain;
        $this->url = $url;
        $this->dn = $dn;
        $this->password = $password;
        $this->baseDn = $baseDn;
    }

    /**
     * @return string
     */
    public function getDomain(): string
    {
        return $this->domain;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getDn(): string
    {
        return $this->dn;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getBaseDn(): string
    {
        return $this->baseDn;
    }

}