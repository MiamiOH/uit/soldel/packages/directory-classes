<?php
/**
 * Created by PhpStorm.
 * User: shettycm
 * Date: 6/29/17
 * Time: 2:38 PM
 */

namespace MiamiOH\Directory;


use LdapTools\LdapManager;
use LdapTools\Object\LdapObject;
use MiamiOH\Directory\Exception\AttributeUpdateNotImplementedException;

class AttributesLdap implements AttributesInterface
{

    /**
     * @var LdapObject
     */
    private $ldapObject;

    /**
     * @var array
     */
    private $replaceAttributes = [];

    public function __construct(LdapObject $ldapObject)
    {
        $this->ldapObject = $ldapObject;
    }

    public function saveWith(LdapManager $ldap): void
    {
//        $this->ldapManagerWrite->persist($entry->getLdapObject());
        
        if (!empty($this->replaceAttributes)) {
            // Reset each of these objects
            foreach (array_keys($this->replaceAttributes) as $attribute) {
                // If the object has the attribute, reset it
                if ($this->ldapObject->has($attribute)) {
                    $this->ldapObject->reset($attribute);
                }
            }
        }
        
        $ldap->persist($this->ldapObject);

        if (!empty($this->replaceAttributes)) {
            // Add each of these replacement objects
            foreach ($this->replaceAttributes as $attribute => $value) {
                if (!empty($value)){
                    $this->ldapObject->add($attribute, $value);
                }
            }
            $ldap->persist($this->ldapObject);

        }

    }

    private function getAttribute(string $attribute, $default = '')
    {
        // TODO use default type (string or array) to ensure correct return type
        if ($this->ldapObject->has($attribute)) {
            $value = $this->ldapObject->get($attribute);

            if (is_array($default) && !is_array($value)) {
                return [$value];
            } elseif (!is_array($default) && is_array($value)) {
                return $value[0];
            }

            return $value;
        }

        return $default;
    }

    private function setAttribute(string $attribute, $value): void
    {
        // Be careful about updating attributes since LDAP can be very particular

        // Don't set an already empty attribute to empty
        if (empty($value) && !$this->ldapObject->has($attribute)) {
            return;
        }

        // If there is not a value and the object has the attribute, reset it
        if (empty($value) && $this->ldapObject->has($attribute)) {
            $this->ldapObject->reset($attribute);
            return;
        }

        // If value is not an array, just do a set
        if (!is_array($value) && $this->ldapObject->has($attribute)) {
            $this->ldapObject->set($attribute, $value);
            return;
        }

        // Add the values if the object does not already have any
        if (!$this->ldapObject->has($attribute)) {
            $this->ldapObject->set($attribute, $value);
            return;
        }

        // Find the diff in arrays (both ways) and add/remove values
        // Store the current values since the object may change
        $currentValues = $this->ldapObject->get($attribute);
        if (!is_array($currentValues)) {
            $currentValues = [$currentValues];
        }

        // Find any values which are being added
        foreach (array_diff($value, $currentValues) as $addValue) {
            $this->ldapObject->add($attribute, $addValue);
        }

        // Find any values which are being removed
        foreach (array_diff($currentValues, $value) as $removeValue) {
            $this->ldapObject->remove($attribute, $removeValue);
        }
    }

    private function replaceAttribute(string $attribute, $value): void
    {
        $this->replaceAttributes[$attribute] = $value;
    }

    public function getBuildingNames(): array
    {
        return $this->getAttribute('buildingName', []);
    }

    public function setBuildingNames(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getFaxNumber(): string
    {
        return $this->getAttribute('facsimileTelephoneNumber');
    }

    public function setFaxNumber(string $data): void
    {
        $this->setAttribute('facsimileTelephoneNumber', $data);
    }

    public function getNameSuffix(): string
    {
        return $this->getAttribute('generationQualifier');
    }

    public function setNameSuffix(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getGivenName(): string
    {
        return $this->getAttribute('givenName');
    }

    public function setGivenName(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getHomePhone(): array
    {
        return $this->getAttribute('homePhone', []);
    }

    public function setHomePhone(array $data): void
    {
        $this->replaceAttribute('homePhone', $data);
    }

    public function getHomePostalAddress(): PostalAddress
    {
        return new PostalAddress(explode('$', $this->getAttribute('homePostalAddress')));
    }

    public function setHomePostalAddress(PostalAddress $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getLocations(): array
    {
        return $this->getAttribute('l', []);
    }

    public function setLocations(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getLabeledUri(): string
    {
        return $this->getAttribute('labeledUri');
    }

    public function setLabeledUri(string $data): void
    {
        $this->setAttribute('labeledUri', $data);
    }

    public function getMail(): string
    {
        return $this->getAttribute('mail');
    }

    public function setMail(string $data): void
    {
        $this->setAttribute('mail', $data);
    }

    public function getMailAliases(): array
    {
        return $this->getAttribute('mailAlternateAddress', []);
    }

    public function setMailAliases(array $data): void
    {
        $this->setAttribute('mailAlternateAddress', $data);
    }

    public function getMailRoutingAddress(): string
    {
        return $this->getAttribute('mailRoutingAddress');
    }

    public function setMailRoutingAddress(string $data): void
    {
        $this->setAttribute('mailRoutingAddress', $data);
    }

    public function getMobileNumber(): string
    {
        return $this->getAttribute('mobile');
    }

    public function setMobileNumber(string $data): void
    {
        $this->setAttribute('mobile', $data);
    }

    public function getAdditionalAffiliations(): array
    {
        return $this->getAttribute('muohioeduAdditionalAffiliations', []);
    }

    public function setAdditionalAffiliations(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getAffiliations(): array
    {
        return $this->getAttribute('muohioeduAffiliation', []);
    }

    public function setAffiliations(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getAffiliationCodes(): array
    {
        return $this->getAttribute('muohioeduAffiliationCode', []);
    }

    public function setAffiliationCodes(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getBuildingCodes(): array
    {
        return $this->getAttribute('muohioeduBuildingCode', []);
    }

    public function setBuildingCodes(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getCampusTelephoneNumber(): string
    {
        return $this->getAttribute('muohioeduCampusTelephoneNumber');
    }

    public function setCampusTelephoneNumber(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getClasses(): array
    {
        return $this->getAttribute('muohioeduClass', []);
    }

    public function setClasses(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getClassCodes(): array
    {
        return $this->getAttribute('muohioeduClassCode', []);
    }

    public function setClassCodes(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getColleges(): array
    {
        return $this->getAttribute('muohioeduCollege', []);
    }

    public function setColleges(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getCollegeCodes(): array
    {
        return $this->getAttribute('muohioeduCollegeCode', []);
    }

    public function setCollegeCodes(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getDepartments(): array
    {
        return $this->getAttribute('muohioeduDepartment', []);
    }

    public function setDepartments(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getDepartmentCodes(): array
    {
        return $this->getAttribute('muohioeduDepartmentCode', []);
    }

    public function setDepartmentCodes(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getDirectoryProxies(): array
    {
        return $this->getAttribute('muohioeduDirectoryProxy', []);
    }

    public function setDirectoryProxies(array $data): void
    {
        $this->setAttribute('muohioeduDirectoryProxy', $data);
    }

    public function getDivisions(): array
    {
        return $this->getAttribute('muohioeduDivision', []);
    }

    public function setDivisions(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getDivisionCodes(): array
    {
        return $this->getAttribute('muohioeduDivisionCode', []);
    }

    public function setDivisionCodes(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getEmployeeSchools(): array
    {
        return $this->getAttribute('muohioeduEmployeeSchool', []);
    }

    public function setEmployeeSchools(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getEmployeeSchoolCodes(): array
    {
        return $this->getAttribute('muohioeduEmployeeSchoolCode', []);
    }

    public function setEmployeeSchoolCodes(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getEmployeeType(): string
    {
        return $this->getAttribute('muohioeduEmployeeType');
    }

    public function setEmployeeType(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getEmployeeTypeCode(): string
    {
        return $this->getAttribute('muohioeduEmployeeTypeCode');
    }

    public function setEmployeeTypeCode(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getGreekOrg(): string
    {
        return $this->getAttribute('muohioeduGreekOrg');
    }

    public function setGreekOrg(string $data): void
    {
        $this->setAttribute('muohioeduGreekOrg', $data);
    }

    public function getHidden(): string
    {
        return $this->getAttribute('muohioeduHidden');
    }

    public function setHidden(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getHigherEducation(): string
    {
        return $this->getAttribute('muohioeduHigherEducation');
    }

    public function setHigherEducation(string $data): void
    {
        $this->setAttribute('muohioeduHigherEducation', $data);
    }

    public function getHighschoolAttended(): string
    {
        return $this->getAttribute('muohioeduHighschoolAttended');
    }

    public function setHighschoolAttended(string $data): void
    {
        $this->setAttribute('muohioeduHighschoolAttended', $data);
    }

    public function getHours(): string
    {
        return $this->getAttribute('muohioeduHours');
    }

    public function setHours(string $data): void
    {
        $this->setAttribute('muohioeduHours', $data);
    }

    public function getLabeledPhotoURL(): string
    {
        return $this->getAttribute('muohioeduLabeledPhotoURL');
    }

    public function setLabeledPhotoURL(string $data): void
    {
        $this->setAttribute('muohioeduLabeledPhotoURL', $data);
    }

    public function getLocationCodes(): array
    {
        return $this->getAttribute('muohioeduLocationCode', []);
    }

    public function setLocationCodes(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getMailStopCodes(): array
    {
        return $this->getAttribute('muohioeduMailStopCode', []);
    }

    public function setMailStopCodes(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getMajors(): array
    {
        return $this->getAttribute('muohioeduMajor', []);
    }

    public function setMajors(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getMajorCodes(): array
    {
        return $this->getAttribute('muohioeduMajorCode', []);
    }

    public function setMajorCodes(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getMesgAddr(): string
    {
        return $this->getAttribute('muohioeduMesgAddr');
    }

    public function setMesgAddr(string $data): void
    {
        $this->setAttribute('muohioeduMesgAddr', $data);
    }

    public function getMiddleInitial(): string
    {
        return $this->getAttribute('muohioeduMiddleInitial');
    }

    public function setMiddleInitial(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getMiddleName(): string
    {
        return $this->getAttribute('muohioeduMiddleName');
    }

    public function setMiddleName(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getMinors(): array
    {
        return $this->getAttribute('muohioeduMinor', []);
    }

    public function setMinors(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getMinorCodes(): array
    {
        return $this->getAttribute('muohioeduMinorCode', []);
    }

    public function setMinorCodes(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getPagerEmail(): string
    {
        return $this->getAttribute('muohioeduPagerEmail');
    }

    public function setPagerEmail(string $data): void
    {
        $this->setAttribute('muohioeduPagerEmail', $data);
    }

    public function getPhotoURL(): string
    {
        return $this->getAttribute('muohioeduPhotoURL');
    }

    public function setPhotoURL(string $data): void
    {
        $this->setAttribute('muohioeduPhotoURL', $data);
    }

    public function getPrimaryAffiliation(): string
    {
        return $this->getAttribute('muohioeduPrimaryAffiliation');
    }

    public function setPrimaryAffiliation(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getPrimaryAffiliationCode(): string
    {
        return $this->getAttribute('muohioeduPrimaryAffiliationCode');
    }

    public function setPrimaryAffiliationCode(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getPrimaryLocation(): string
    {
        return $this->getAttribute('muohioeduPrimaryLocation');
    }

    public function setPrimaryLocation(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getPrimaryLocationCode(): string
    {
        return $this->getAttribute('muohioeduPrimaryLocationCode');
    }

    public function setPrimaryLocationCode(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getProjects(): string
    {
        return $this->getAttribute('muohioeduProjects');
    }

    public function setProjects(string $data): void
    {
        $this->setAttribute('muohioeduProjects', $data);
    }

    public function getRecordInactive(): string
    {
        return $this->getAttribute('muohioeduRecordInactive');
    }

    public function setRecordInactive(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getVPNGroup(): string
    {
        return $this->getAttribute('muohioeduVPNGroup');
    }

    public function setVPNGroup(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getPagerNumber(): string
    {
        return $this->getAttribute('pager');
    }

    public function setPagerNumber(string $data): void
    {
        $this->setAttribute('pager', $data);
    }

    public function getNamePrefix(): string
    {
        return $this->getAttribute('personalTitle');
    }

    public function setNamePrefix(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getPostalAddress(): PostalAddress
    {
        return new PostalAddress(explode('$', $this->getAttribute('postalAddress')));
    }

    public function setPostalAddress(PostalAddress $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getFamilyName(): string
    {
        return $this->getAttribute('sn');
    }

    public function setFamilyName(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getTelephoneNumber(): array
    {
        return $this->getAttribute('telephoneNumber', []);
    }

    public function setTelephoneNumber(array $data): void
    {
        $this->replaceAttribute('telephoneNumber', $data);
    }

    public function getTitles(): array
    {
        return $this->getAttribute('title', []);
    }

    public function setTitles(array $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getUniqueId(): string
    {
        return $this->getAttribute('uid');
    }

    public function setUniqueId(string $data): void
    {
        throw new AttributeUpdateNotImplementedException();
    }

    public function getDisplayName(): string
    {
        return $this->getAttribute('displayname');
    }

    public function setDisplayName(string $data): void
    {
        $this->setAttribute('displayname', $data);
    }

    public function getEntitlements(): array
    {
        return $this->getAttribute('eduPersonEntitlement', []);
    }

    public function setEntitlements(array $data): void
    {
        $this->setAttribute('eduPersonEntitlement', $data);
    }

}
